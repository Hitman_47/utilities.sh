#! /bin/bash
# This script can be launched as a normal user or as root, inside of a terminal or outside of one. Running it with no arguments launches the option selectors and/or the GUI. If a root option is selected, the script will launch another instance of itself as root to take the argument, which allows easy creation of a sudoers rule.

################################################## main variables and functions

quiet=/dev/null # misc. garbage redirections not meant for the log, point to /dev/stderr for debugging
script_path="$(realpath "${BASH_SOURCE:-$0}")" DIR="$( cd "$( dirname "$script_path" )" >$quiet 2>&1 && pwd )" # determine path of the script and directory
config_vars='^(fans|xenv|sudoers|showopts|scrollback|customtheme|autorefresh|terminal|use_gui|main_return)=[[:print:]].*$'
xenv_vars='^(XDG_CURR|XDG_SESS|QT_|GTK_|DISP|WAY|WLR).*=[[:print:]]'
branding='Utilities' # Name for the script's title and log files
generictext='Choose desired option:' # text for a generic menu
lf=/tmp/$branding.log # log file location
readconf() { [[ -f "$DIR"/utconfig ]] && for var in $(grep -E "$config_vars" "$DIR/utconfig" | tr -d '><\\})"|^#%({&!*@$'); do declare -g $var; done; } # read variables from config file
writeconf() ( typeset | grep -E "$config_vars" > "$DIR"/utconfig ) # export config

# menu creation
testcmd() { command -v "$1" >$quiet 2>&1; } # test a command
getlines() { tput lines 2>$quiet || stty size 2>$quiet | awk '{ print $1 } END { if (!NR) print "25" }'; } # get amount of lines
getcols() { [[ -n $width ]] && echo $(( $width / 40 )) && return; tput cols 2>$quiet || stty size 2>$quiet | awk '{ print $2 } END { if (!NR) print "80" }'; } # get amount of columns
separator() { n=$(getcols); nr="$1"; [[ -n $nr ]] && n=$(( $n / $(tr -dc '0-9' <<<$nr) )); echo "$(printf %"$n"s | tr ' ' '#')"; } # insert a separator in output text
menu_OK() { let nloops=nloops+1; [[ $nloops -lt 45000 ]] && [[ $(sed -ne 's/[^0-9]//g; 3p' /proc/meminfo) -gt 20000 ]]; } # safety system for the menus
termslp() { ec="$?"; [[ $ec -gt 0 ]] && echo -n "[$ec] "; ! [ -t 0 ] && return $ec; read -r -t180 -n1 -p 'Press any key to return...'; echo -e '\e[H\e[J'; return $ec; } # delay in a terminal
returntomain() { if [[ -n $main_ran ]]; then selmain; elif ! $main_return; then exit; elif [ -t 0 ]; then selmain; else exit; fi; } # determine how to take the main menu option
input_OK() { ! [[ "$1" =~ ['!@#$%^&*({})|+'] ]]; } # test if an input has desirable text
# system info part 1
partitions() { lsblk -rnp | grep ' part' | grep -vE '/efi| swap|SWAP| /$' | cut -d' ' -f1; }; drives_all() { lsblk -p | grep '^/dev/.*disk' | grep -vE 'SWAP| dm| lvm|\s0B' | cut -d' ' -f1; }; bootdisk=$(lsblk -no pkname "$(df / --output=source | tail -n1)" 2>$quiet); drives() { drives_all | grep -v "$bootdisk"; } # list nonsystem partitions and disks
cpus=$(( $(cut -d'-' -f2 /sys/devices/system/cpu/present) + 1 )) # number of CPU threads
init=$(if [[ -s /etc/inittab ]] || [[ $(head -n1 /sbin/init) == *sh* ]]; then grep -oE '(/usr/bin|/sbin)/[[:alnum:]]{1,}' /etc/inittab /sbin/init 2>$quiet | cut -d: -f2 | sort -u; else echo '/sbin/init'; fi | xargs -r strings | grep -oE 'OpenRC|Dinit|runit|s6-linux-init' | head -n1) # determine init

# have the script know about it's surroundings
user_curr=$(whoami || echo $USER); runningasroot() { [ "$user_curr" = root ]; }; testcmd doas && sub=doas; testcmd sudo && sub=sudo
autorefresh=; scrollback=; use_gui=; argcontinue=; i=1 nloops=1 menudelay=2; readconf # config init
stat "$script_path" | grep -q 'x[rw-]*x' || chmod +x "$script_path" # make the script executable if it was launched externally by a shell
getuserhome() { userhome=$(getent passwd "$user_tgt" | cut -d: -f6); [[ $(wc -c <<<$userhome) -gt 3 ]] && ls $userhome >$quiet 2>&1 && unset nouser; } # verify a target user and get it's homedir
writelog() { etx=( "$@" ); if [[ -z ${etx[0]} ]]; then tee -a "$lf"; else echo "${etx[@]}"; text="$(date +"%m-%d-%Y %H:%M") $user_curr ${etx[*]}"; ! diff -q <(tail -n1 "$lf") <(echo -e "$text") >$quiet && echo -e "$text" >> "$lf"; fi; return 0; } # function writelog works both from stdin, and with arbitrary text as an argument
if runningasroot; then
	touch "$lf"; chown root "$lf"; chmod 777 "$lf"; echo -n '' >> "$lf" || lf=$(sed 's/\.log/_root\.log/g' <<<$lf)
	getuid() { grep -E "$1.*:1[0-9]{3,5}" /etc/passwd | cut -d: -f3; }; getuser() { grep -E "$1:1[0-9]{3,5}" /etc/passwd | cut -d: -f1; } # get norm. uid/user from arg.
	[[ -n $user_tgt ]] && ! getuserhome && writelog "Could not determine paths for user $user_tgt" && sleep 2 && unset user_tgt # look if the target user variable was set
	[[ -z $nouser ]] && [[ -z $user_tgt ]] && for i in $(users | tr ' ' '\n'; getuser); do uid=$(getuid "$i"); [[ $uid -lt 1000 ]] && continue; user_tgt=$i; pgrep -U "$uid" "$(basename "$script_path")" >$quiet 2>&1 && getuserhome && break; getuserhome && break; done # find a normal user otherwise
	[[ -z $user_tgt ]] && user_tgt=root userhome=/tmp nouser=1 # if either fails the script will run root only
	grep -sq "$script_path" /etc/sudoers.d/utilities && ( lsattr "$script_path" | grep -q '\-i' || chattr +i "$script_path" ) # check for the immutable attr if the sudoers rule is present
	on_x11() { ! [[ $xenv == *wayland* ]]; }
	[[ -z $nouser ]] && runfromroot="$sub -u $user_tgt env XDG_RUNTIME_DIR=/run/user/$(getuid $user_tgt) XAUTHORITY=$(on_x11 && find /run/user/[0-9]* /tmp $userhome -maxdepth 1 -type f -regex '\(\.Xauthority\|.*auth[_\-\.].*\)' -size +0 -user "$user_tgt" 2>$quiet | head -n1) XDG_CONFIG_HOME=$userhome/.config $(tr ',' '\n' <<<$xenv | grep -E "$xenv_vars" | tr '\n' ' ') XDG_CACHE_HOME=/tmp/" # run a command from root with the GUI environment retrieved
	has_sudoers_attr() { args_rootonly -testrule >$quiet; }
else
	echo -n '' >> "$lf" || lf=$(sed "s/\.log/_$user_curr\.log/g" <<<$lf)
	[[ -n $user_tgt ]] && getuserhome || user_tgt=$user_curr userhome=~
	testcmd pkexec && ppr='pkexec'; testcmd lxqt-sudo && ppr='lxqt-sudo' # determine password prompt program
	on_x11() { ! [ "$XDG_SESSION_TYPE" = wayland ]; }
	has_sudoers_attr() { lsattr "$script_path" | grep -q '\-i' && timeout 1 sudo "$script_path" -testrule >$quiet 2>&1; }
fi

# obtain screen resolution
[[ -z $nouser ]] && [ ! -t 0 ] && for dv in $({ on_x11 && xprop -root _NET_DESKTOP_GEOMETRY 2>$quiet | cut -d= -f2 || find -L /sys/class/drm -maxdepth 6 -name virtual_size -exec cat {} \; 2>$quiet; } | awk -F, 'NR==1{print "width="int($1)"\nheight="int($2)}'); do declare $dv; done
# screen size for GUI menus
if [[ -n $width ]] && [[ -n $height ]]; then
szl=$(awk -v CONVFMT='%.0f' BEGIN'{print ('$width'/2.7)" "('$height'/1.4)}') # list menus
szs=$(awk -v CONVFMT='%.0f' BEGIN'{print ('$width'/3.5)" "('$height'/3)}') # small menus
szi=$(awk -v CONVFMT='%.0f' BEGIN'{print ('$width'/2)" "('$height'/1.5)}') # info menus
else [ ! -t 0 ] && cmdonly=1; fi

# Determine terminal emulator gui, use with argument to run a terminal only option if/when needed
checktermgui() { [ -t 0 ] || [[ -n $nouser ]] && return 1; testcmd $terminal || unset terminal; tex() { [ "$tr" = 1 ] && return 1; [[ -z $terminal ]] && testcmd $1 && terminal=$1; [ "$1" = "$terminal" ] && tr=1; tmtxt=$(tr -d ' ' <<<$branding); }
tex alacritty && term="$_ -t $tmtxt -e"; tex kgx && term="$_ -T $tmtxt -e"; tex kitty && term="$_ -T $tmtxt"; tex konsole && term="$_ -p tabtitle=$tmtxt --nofork -e"
tex lxterminal && term="$_ -t $tmtxt -e"; tex qterminal && term="$_ -e"; tex terminator && term="$_ -T $tmtxt -x"; tex tilix && term="$_ -t $tmtxt -e"; tex xfce4-terminal && term="$_ -T $tmtxt -x"
earg=( "$@" ); [[ -n "${earg[0]}" ]] && if [[ -n $term ]] && [[ -n $DISPLAY ]] && $use_gui; then $term "$script_path" "${earg[@]}" 2>$quiet || writelog "Cannot open a terminal emulator for option $earg"; elif [ ! -t 0 ]; then writelog "$earg requires running in a terminal"; fi; }

# system info part 2, summary to be placed in main menu and terminal footer
getsysinfo() {
sysinfo="[ $( { cat /proc/loadavg /proc/uptime; head -n1 /proc/stat; sleep .045 && head -n1 /proc/stat; free | sed -n '2p'; cat /sys/class/power_supply/*/capacity /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_cur_freq; } 2>$quiet |
awk -v cpus="$cpus" -v CONVFMT='%.1f' '(NR==1){loadavg=$2};(NR==2){up=int($1)};(NR==3){u1=$2+$4;t1=$2+$4+$5};(NR==4){u=$2+$4;t=$2+$4+$5};(NR==5){avail=$7;cch=$6};(NR>=6){if ($1 <=100) bat=$1; else if (length(min) == 0) {min=$1; max=$1}; if(min>$1) min=$1; if(max<$1) max=$1};\
END{printf "RAM avail. "(avail/1048576)"G (cache ";if (cch > (avail*0.95)) printf "full)"; else printf sprintf("%.0f",cch/avail*100)"%%)";\
printf ", CPU "; if (t <= t1) printf 100; else if (u <= u1) printf 0; else printf sprintf("%.0f",( (u-u1) * 100 / (t-t1) )); printf "%% "(min/1000000)"~"(max/1000000)"GHz";\
printf " (avg. ";if (loadavg > cpus) printf "! overload +"sprintf("%.0f",(loadavg-cpus)/cpus*100)"%%"; else printf sprintf("%.0f",loadavg/cpus*100)"%%)";\
printf ", Uptime %02d:%02d:%02d", up/3600, (up/60)%60, up%60; if (bat>0)printf ", Bat. "bat"%"}' ) ]"
}
sysinfo_static="[ $( { uname -rm & awk '/MemTotal/{print ",RAM:"int($2/1000000)"G,"}; /^(VERSION|NAME|VARIANT|BUILD_ID)=/{print $0","}; END{print "CPU-thds.:'$cpus'"}' /proc/meminfo /etc/*-release | cut -d'=' -f2; } | tr -d '"\n' ) ]"

# get main menu options
mainmenu() {
getsysinfo; ds=$(drives | wc -l) log=$(tail -n200 "$lf" 2>$quiet); getastats
createarray <<<"$(
echo -n "# -> System - Mount/filesystem tools"; [[ $ds -gt 0 ]] && echo -n " [ $ds drive$([[ $ds -gt 1 ]] && echo -n 's') available ]"
[[ -n $init ]] && echo -e "\n# -> System - [$init] service manager"
testcmd pacman || testcmd apk || testcmd opkg && echo -e '\n  -> System - Package management tools'
testcmd nft && echo -ne "\n# -> System - Quick nftables firewall mgr." && [[ $log == *\!\!\ IP* ]] && echo -n ' [(!) IP updater had a problem]'; testcmd wg && wg show interfaces | sed -ne 's/^.*$/ [WG connected to &]/; 1p'
)"
menu+=(
"# -> System - Swap manager$(free -h | awk 'END{if (int($2) > 0) gsub("i",""); print " [ Used "$3" / "$2" ]"}')"
"# -> System - Kernel tools"
"# -> System - Maintenance"
"$([[ -z $SSH_TTY ]] && echo -n '  -> System - Display tools' && pgrep 'x0vncserver|wayvnc' -U $user_tgt >$quiet 2>&1 && echo -n ' [ VNC Server running ]')"
"# -> Hardware - Info, diagnostics and benchmarks"
"# -> Hardware - Fan control"
"$(ls /sys/bus/usb/devices/*:* >$quiet 2>&1 && echo -n '# -> Hardware - USB device manager')"
"# -> Hardware - Parameters$(gpustats info && sed 's/\]//g; s/^.*$/ [GPU: &]/g' <<<$gpuinfo)"
"  -> Hardware - Audio stats & volume control$([[ -n "$astats" ]] && echo -n " [$astats" | tr '\n' ',' && echo -n ']')"
"  -> Hardware - Sensors and I/O monitors"
"  -> Disk - Archiving/backup tools"
"  -> Disk - Storage space analyzer"
"  -> Disk - File manager"
"  -> Disk - Downloader tools"
"  -> Launcher - Misc. tools"
)
createarray add <<<"$(
[ ! -t 0 ] && echo '  -> Launcher - Wine custom version/prefix quick deploy'
testcmd qemu-system-x86_64 && echo '# -> Launcher - QEMU VM quick deploy'
if runningasroot; then [[ -z $nouser ]] && echo "  -> User determined: [$user_tgt]. Select to specify another" || echo '  -> Choose a normal user'; [[ -z $nouser ]] && echo " .:: Run the script as normal user [$user_tgt]"; elif [ -t 0 ]; then $use_gui && [[ -z $SSH_TTY ]] && echo ' .:: Use the GUI'; else echo ' .:: Use the console'; fi
if runningasroot; then [[ -z $nouser ]] && if has_sudoers_attr; then echo "  -> Remove sudoers rule for user [$(cut -d' ' -f1 </etc/sudoers.d/utilities)]"; elif testcmd sudo; then echo "  -> Add sudoers rule for user [$user_tgt]"; fi; elif [ -t 0 ]; then echo ' .:: Run in root mode'; else echo ' .:: Use the console (root mode)'; fi)"
menu+=(
" .:: Preferences"
"  -> Exit"
"$([ ! -t 0 ] && echo "$sysinfo"
$use_gui && [[ -n $cmdonly ]] && writelog '[ ! Could not detect display parameters, script started in terminal only mode ]' &
df -h --total | awk 'BEGIN{w="(!) "; printf "[ Storage:"};\
/\/$/{fs=$1;fsh=$1;s=$2;pr=int($5);if ( pr > 90 ) u=w$3; else u=$3};\
/'"$(basename $userhome)"'$/{fsh=$1;sh=$2;if ( int($5) > 90 ) uh=w$3; else uh=$3};\
END{pti=int($5);if ( pti > 90 ) pt=w$5; else pt=$5; if ( fsh != fs ) printf " home used "uh" / "sh";"; if ( (pti-pr) !~ /[012-]/ ) printf " root used "u" / "s";"; print " total on system: "$2", used: "pt" ]"}'
rt=$(ip route show table all | head -c50); awk -v CONVFMT='%.2f' 'BEGIN{print "[ Network interfaces: ]"}; !/\slo:/{if (int($2)) print $1,($2/1024/1024/1024)"G ↓, "($10/1024/1024/1024)"G ↑"}' /proc/net/dev 2>$quiet | while read -r c; do [[ $rt == *default*$(tr -d ':' <<<"$c" | cut -d' ' -f1)* ]] && echo "$c [Default route]" || echo "$c"; done)"
)
main_ran=1
}

################################################## Main option loop

selmain() {

while menu_OK; do
title='Main menu' header="$generictext (# - requires root)\nOptions may appear or dissapear depending on the packages installed or hardware."
getnp; if ! $showopts; then [[ -n $tc ]] && tc=$(grep -v OPTIONAL <<<$tc); fi; checktermgui

# Determine how to open the main menu
if [ -t 0 ]; then # terminal
readconf
[[ -n $tc ]] && echo -e "\nMissing packages:\n$tc\n" && [[ $tc == *OPTIONAL* ]] && showopts=true && read -rp 'Show optional packages next time? (Y/n)' q && if [[ $q =~ y|Y|^$ ]]; then showopts=true; else showopts=false; fi && writeconf # display warning if packages are missing
sel mainmenu

elif ! runningasroot; then
if testcmd zenity || testcmd kdialog && [[ -z $cmdonly ]] && $use_gui; then # GUI
pgrep -U $user_tgt -f '(zenity|kdialog).*[t]itle.{,2}(SETUP|Main menu)' && writelog 'Another instance is already running' && exit # avoid having a mess of multiple instances at the main menu
readconf
main_return=; xenv=$(env | grep -E "$xenv_vars" | tr '\n' ',') # get GUI environment to be used later by root section
gui_sudoers() { if has_sudoers_attr; then sudoers=true; elif $sudoers; then sudoers=false; testcmd sudo && if text='Add a rule in sudoers.d to bypass password prompts? To protect the script from being tampered it will receive the "immutable" attribute' title='SETUP - Bypass prompt for root options' gui_question; then $ppr env user_tgt="$user_tgt" "$script_path" -addrule && sudoers=true; else sudoers=false; fi; fi; } # look for sudoers rule if a root option was selected
if [[ -n $tc ]]; then text="$tc" title="Missing packages:" gui_warn || exit; [[ $tc == *OPTIONAL* ]] && showopts=true && if text='Show optional packages next time?' title='SETUP - Optionals' gui_question; then showopts=true; else showopts=false; fi; fi # display warning if packages are missing
sel mainmenu $szl

elif [[ -n $term ]] && [[ -z $cmdonly ]]; then $term "$script_path" 2>$quiet & disown & exit 0
elif [[ -n $term ]]; then cmdonly=1 $term "$script_path" 2>$quiet & disown & exit 1 # if display vars cannot be detected open terminal mode with a warning
else writelog 'No terminal emulator found'; exit 1; fi
else writelog 'Script needs to run in a TTY'; exit 1; fi

# Arguments for the selectors
run_rootonly() { if ! runningasroot; then main_return=false; writeconf; if [ ! -t 0 ]; then gui_sudoers; if ! $sudoers && [[ -z $rnogui ]]; then $ppr env user_tgt="$user_tgt" nohup "$script_path" "$@" >$quiet; else $(! $use_gui || [[ -n $rnogui ]] && echo -n $term) $sub "$script_path" "$@"; fi; else $sub "$script_path" "$@"; fi; else args_rootonly "$@"; fi; }
cd /tmp && unset opt_file &&
case $opt in
	*\ Mount*) run_rootonly -mnt;;
	*\ Swap\ manager*) run_rootonly -swp;;
	*\ service*) run_rootonly -srv;;
	*\ Fan*) run_rootonly -fancontrol; [ "$?" = 130 ] && exit 0 || readconf;;
	*\ USB*) run_rootonly -usb;;
	*\ Kernel*) run_rootonly -kern;;
	*\ Parameters*) run_rootonly -hwp;;
	*\ VM*) run_rootonly -qemu;;
	*\ firewall*) rnogui=1 run_rootonly -fw;;
	*\ Info*) rnogui=1 run_rootonly -diags;;
	*\ Maintenance*) rnogui=1 run_rootonly -maint;;
	*\ Wine*) args_useronly -wine;;
	*\ Display*) args_useronly -disptools;;
	*\ Misc*) args_useronly -customcommands;;
	*\ analyzer) args_all -span;;
	*\ File*) args_all -fm;;
	*\ Preferences) args_all -prefs;;
	*\ Audio*) args_all -audiomgr;;
	*\ Package*) args_all -pkgutils;;
	*\ Downloader*) args_all -dltools;;
	*\ Archiving*) args_all -arc;;
	*\ I/O*) args_all -iomons;;
	# internal usage
	*\ GUI) nohup "$script_path" >$quiet & sleep .5 && exit 0;;
	*\ console) $term "$script_path" 2>$quiet & disown 2>$quiet & exit 0;;
	*\ normal\ user|*\ another) ulist=$(getuser); [[ -z $ulist ]] && continue; createarray <<<$ulist; text='Choose a normal user' sel; user_tgt=$opt; getuserhome || user_tgt=root userhome=/tmp nouser=1;;
	*\ Run\ *user*) $runfromroot "$script_path";;
	*root\ mode*) rnogui=1 run_rootonly;;
	*\ Add\ sudoers*) args_rootonly -addrule;;
	*\ Remove\ sudoers*) args_rootonly -rmrule;;
	*\>\ Exit*|"") writeconf; exit 0
esac
ec=$?; case $ec in 0) continue;; 130) writeconf; exit 0;; *) if [ -t 0 ]; then echo "[$ec] An error occured" && termslp; else text="[$ec] An error occured" gui_warn; fi; esac # exit code 130 (same as ctrl c) reserved for manually exiting, otherwise run the selectors again
done
}

################################################## Root only arguments

args_rootonly() {

case "$1" in

-diags)
checktermgui $1 && return
while menu_OK; do
createarray<<<"
-> [lspci] Show PCI devices and kmods associated to them
-> [dmidecode] RAM info
-> [inxi] Print full system specs with peripherals
-> [smartctl] SMART data for every disk
-> [hdparm] Simple I/O read benchmark for every disk (one by one)
-> [hdparm] Simple I/O read benchmark for every disk (parallel - might affect results)
-> [fio] I/O read+write benchmark
-> [iperf] Host network benchmark
-> [iperf] Connect to host
-> [memtester] Run a memory test loop
-> [mprime] Stress test
-> [unixbench] Run whet/dhry benchmark
-> [sysbench] Run cpu test on all threads
-> Back"
title='Info, diagnostics and benchmarks' sel
case $opt in
	\!*) continue;;
	*hdparm*one*) drives_all | while read -r d; do hdparm -t "$d"; separator; done | less -K -O$quiet;;
	*hdparm*parallel*) drives_all | while read -r d; do { hdparm -t "$d"; separator; } > /tmp/bench_"$(basename "$d")" & done; wait; cat /tmp/bench_* | less -K -O$quiet;;
	*smartctl*) drives_all | while read -r d; do smartctl -a $d; separator; done | less -K -O$quiet;;
	*memtester*) echo "Select memory size to test in GB, RAM currently available: $(free -g | awk 'NR==2{print $7" GB"}')"; read -r size; [[ -n $size ]] && memtester "$size"G | less -K -O$quiet;;
	*fio*)
	text='Select location for the benchmark:' file_browser --dir; [[ ! -d "$opt_file" ]] && exit
	cd "$opt_file" && fio --name TEST --filename=fio.tmp --rw=write --size=3g --blocksize=512k --ioengine=libaio --iodepth=32 --direct=1; separator; fio --name TEST --filename=fio.tmp --rw=read --size=3g --blocksize=512k --ioengine=libaio --iodepth=32 --direct=1; separator; rm fio.tmp; termslp;;
	*lspci*) lspci -v | less -K -O$quiet;;
	*dmidecode*) dmidecode --type 17 | less -K -O$quiet;;
	*inxi*) inxi -FdjJlmpzxx --dmidecode | less -K -O$quiet;;
	*iperf*Host*) echo 'Address(es) of this host:'; ip addr | grep -v ' lo' | awk -F 'inet ' '{print $2}' | sed -e '/^$/d; s/\/.*//g'; iperf -e -s;;
	*iperf*Connect*) read -rp 'Choose host to connect to:' hst; [[ -n $hst ]] && iperf -e -c "$(tr -dc '[:alnum:]./:' <<<$hst)";;
	*mprime*) mprime -t | less -K -O$quiet;;
	*unixbench*) ubench whets dhry | less -K -O$quiet; find '/usr/bin/unixbench' -name "$(hostname)*" -exec mv -v '{}' /tmp/ \; ;;
	*sysbench*) sysbench --threads=$cpus cpu run; termslp;;
	*) returntomain
esac; done
;;

-qemu)
vm_mem=2G vm_cpus=$(( $cpus / 2 )) bios=true spice=true mcomp=false; testcmd remote-viewer || spice=false; [[ -z $SSH_TTY ]] && rtext=' -\> Connect to' || rtext='Running:'; runningvms() { pgrep -fa 'qemu.*name VM:'; }
runvm() {
nr=$(runningvms | wc -l); adv() { ss -lnt | grep -oqE ":(333|444|555|590)$nr\s" && let nr=nr+1; }; adv && adv && adv && writelog 'No free ports' && return 1
qemu-system-x86_64 -m "$vm_mem" -smp "$vm_cpus" -monitor telnet:localhost:555$nr,server,nowait -serial telnet:localhost:444$nr,server,nowait -name VM:"$nr"_$(basename $disk1 2>$quiet | tr -dc '[[:alnum:]]_.'; date '+_%d_%b_%H.%M.%S' | tr -d '\n'
[[ -f "$disk1" ]] && echo -n " -drive file=$disk1"; [[ -f "$disk2" ]] && echo -n " -drive file=$disk2"; [[ -f "$disk3" ]] && echo -n " -drive file=$disk3"
ls /dev/kvm >$quiet 2>&1 && echo -n ' -cpu host -enable-kvm'; ! $bios && echo -n " -bios $ovmf"
$spice && echo -n " -spice port=333$nr,disable-ticketing=on" || echo -n " -vnc :$nr"
$mcomp && echo -n ' -device usb-ehci,id=usb,bus=pci.0,addr=0x4 -device usb-tablet')
}
checktermgui; while menu_OK; do
ovmf=$(find /usr/share -maxdepth 4 -type f -name 'OVMF.fd' -o -name 'OVMF.4m.fd' | tail -n1)
vmmenu() { createarray<<<"$(sep=$(separator /2)
runningvms | while read -r i; do echo "$sep"; grep -oE 'name [[:print:]]{1,}' <<<$i | cut -d' ' -f2 & { grep -oE 'monitor telnet:localhost:[0-9]{1,}' <<<$i | tr -dc '0-9\n' | sed 's/^/	'"$rtext"' qemu monitor telnet console on port /' & grep -oE 'serial telnet:localhost:[0-9]{1,}' <<<$i | tr -dc '0-9\n' | sed 's/^/	'"$rtext"' serial telnet console on port /' & grep -oE 'spice port=[0-9]{1,}' <<<$i | tr -dc '0-9\n' | sed 's/^/	'"$rtext"' SPICE viewer on port /' & grep -oE 'vnc :[0-9]{1,}' <<<$i | tr -dc '0-9\n' | sed 's/^/	'"$rtext"' VNC viewer on port 590/'; } | sort; done; echo $sep
)
 -> Boot independently all iso/img/qcow2 disk images from a directory
 -> Create new qcow2 image
 -> Create new generic image
$(
[[ -f $ovmf ]] && echo "BIOS type: $($bios && echo -n legacy || echo -n uefi)"
$spice && echo 'Graphics: Using SPICE, press to use VNC instead' || echo 'Graphics: Use SPICE'
! $mcomp && echo "Pointing: Use mouse workaround for some problematic client GUI's" || echo "Pointing: Don't use workaround"
)
Memory: $vm_mem
CPUs: $vm_cpus
Disk image 1 (primary): $disk1
Disk image 2: $disk2
Disk image 3: $disk3
 -> Boot with above image selection
 -> Back"
}
title='QEMU VM quick launcher' sel vmmenu $szl
case $opt in
	*\ Boot*\ images*) text='Choose directory' file_browser; find "$opt_file" -type f -iregex '.*\.\(iso\|img\|qcow2\)' | while read -r f; do disk1="$f" runvm && sleep .5 & disown; done;;
	*Create\ generic*) text='Size in GB:'; [ -t 0 ] && read -rp "$text" isz || isz=$(gui_textentry); text='Save new generic image:' file_browser; fallocate -l "$isz"G "$opt_file";;
	*Create\ qcow2*) text='Size in GB:'; [ -t 0 ] && read -rp "$text" isz || isz=$(gui_textentry); text='Save new qcow2 image:' file_browser; qemu-img create -f "$opt_file" "$isz"G;;
	*\ Connect\ *VNC*[0-9]) $runfromroot vncviewer localhost\:"$(tr -dc '0-9' <<<"$opt")" & disown;;
	*\ Connect\ *SPICE*[0-9]) $runfromroot remote-viewer spice\:\/\/localhost\:"$(tr -dc '0-9' <<<"$opt")" & disown;;
	*\ telnet\ console*[0-9]) p=$(tr -dc '0-9' <<<"$opt"); [ -t 0 ] && telnet localhost $p || $runfromroot $term telnet localhost $p & disown;;
	BIOS*) $bios && bios=false || bios=true;;
	Pointing*) $mcomp && mcomp=false || mcomp=true;;
	Graphics*) $spice && spice=false || spice=true;;
	Memory*) text="Input the new amount or press m to max out:"; if [ -t 0 ]; then read -rp "$text " mv; else mv=$(gui_textentry); fi; [[ -z $mv ]] && continue; [[ $mv = m ]] && vm_mem=$(awk '/MemTotal/{printf int($2/1048576)"G"}' /proc/meminfo) || vm_mem=$(tr -dc '0-9MG' <<<$mv);;
	CPUs*) text="Input the new amount or press m to max out:"; if [ -t 0 ]; then read -rp "$text " cv; else cv=$(gui_textentry); fi; [[ -z $cv ]] && continue; [[ $cv = m ]] && vm_cpus=$cpus || vm_cpus=$(tr -dc '0-9' <<<$cv);;
	Disk\ image\ 1*) file_browser; disk1=$opt_file;;
	Disk\ image\ 2*) file_browser; disk2=$opt_file;;
	Disk\ image\ 3*) file_browser; disk3=$opt_file;;
	*\ Boot*\ above\ *) runvm & disown; sleep .5; termslp;;
	\#*) continue;;
	*) returntomain
esac
done
;;

-swp)
mkmzram() (
modprobe zram || exit 1
sz1=$(awk '/MemTotal/{printf int($2*1024/12)}' /proc/meminfo) sz2=$(awk '/MemTotal/{printf int($2*1024/6)}' /proc/meminfo)
if ls /dev/zram0 >$quiet 2>&1 && ! swapon --show --noheadings | grep -q /dev/zram0; then echo lzo-rle > /sys/block/zram0/comp_algorithm && echo "$sz1" > /sys/block/zram0/disksize && mkswap -L utswp_lzr /dev/zram0; else zramctl --find --size "$sz1" -a lzo-rle --streams $cpus | xargs mkswap -L utswp_lzr; fi
zramctl --find --size "$sz2" -a zstd --streams $cpus | xargs mkswap -L utswp_zst
swapon -d -p 32767 -L utswp_lzr && swapon -d -p 32766 -L utswp_zst && writelog "Zram created"; ! $argcontinue && exit
)
[[ $2 = zram ]] && argcontinue=false mkmzram
swpmenu () { createarray <<<"
 -> Create a swapfile
 -> Free up /tmp & caches
$(testcmd zramctl && ! swapon --show=LABEL | grep -q utswp && echo -e ' -> Make zram swap with 1/4 of ram (mix of lzo-rle+zstd)\n -> Make zram swap with 1/4 of ram (system default compressor)'
[[ -n $(swapon --show) ]] && echo -e "[ Available swaps, sorted by highest priority, press to empty or turn off: ]\n$(separator /2)" && swapon --show --noheadings | sort -k5r | awk '{print $1, $2, $3,"Used: "$4}')
 -> Main menu
 -> Exit
"; }
title="Swap manager"; while menu_OK; do sel swpmenu $szs
case $opt in
	*Make\ *lzo*)
	mkmzram;;
	*Make\ *default*)
	modprobe zram && zramctl --find --size "$(awk '/MemTotal/{printf int($2*1024/4)}' /proc/meminfo)" --streams "$cpus" | xargs mkswap -L utswp_def; swapon -d -p 32767 -L utswp_def;;
	*Create\ *)
	text='Choose a directory to place swapfile in:' file_browser --dir; [[ -d "$opt_file" ]] && sdir=$opt_file || continue
	[ -t 0 ] && read -rp 'Swapfile size in GB: ' swap || swap=$(val='4' min='2' max='32' text='Choose swapfile size in GB' gui_slider)
	[[ -n $swap ]] && fallocate -l "$swap"G "$sdir"/swapfile && chmod 600 "$sdir"/swapfile && mkswap "$sdir"/swapfile && swapon "$sdir"/swapfile || exit $?
	;;
	*/tmp*)
	mv -f /tmp/*auth* "$DIR"; find '/tmp/' -mindepth 1 -not -name '*auth*' -delete; sync; echo 3 > /proc/sys/vm/drop_caches; mv "$DIR"/*auth* /tmp;;
	*\ file*)
	sfile=$(cut -d' ' -f1 <<<"$opt"); swapoff "$sfile" && rm -f "$sfile";;
	*\ partition*)
	swapoff "$(cut -d' ' -f1 <<<"$opt")"; [[ $opt == *zram* ]] && zramctl -r "$(grep -o '/dev/zram[0-9]' <<<"$opt")"; [[ $opt == *sd* ]] || [[ $opt == *nvm* ]] && swapon -a;;
	*Main\ *|"") returntomain;;
	*Exit) exit 130
esac
done
;;

-kern*)
kmenu() { createarray <<<"
 -> View and modify kernel parameters
 -> Manage kernel modules [ Loaded: $(wc -l </proc/modules) ]
$(testcmd dkms && testcmd mkinitcpio && echo ' -> Refresh dkms+depmod and run mkinitcpio for all available kernels'
testcmd grub-mkconfig && echo ' -> Refresh Grub config'
testcmd grub-install && [[ -n $(ls /boot/initramfs* 2>$quiet) ]] && echo ' -> Run Grub installer'
testcmd refind-install && echo ' -> Run rEFInd installer'
[[ -f /etc/modules-load.d/utilities_added.conf ]] && echo ' -> Remove kmods added on boot by this script')"
menu+=(
"#
	Info:
Booted: $(cut -d' ' -f1 /proc/cmdline | cut -d= -f2-) - [ $(uname -rsv) ]
$(for k in /boot/vmli*; do ver=$(find /lib/modules -maxdepth 2 -size "$(du -b "$k" | cut -f1)"c -name 'vmli*' -printf "%h" | grep -oE '[0-9].*'); echo "$(basename "$k") [ $ver ]" & done | sed '1i\	Kernels available for this system:' &
grep "\smenuentry.*$(grep '^NAME=' /etc/*-release | cut -d= -f2 | tr -d '"')" /boot/grub/grub.cfg 2>$quiet | grep -v fallback | cut -d"'" -f2 | sed '1i\	Kernels set to boot for this system in grub.cfg:' &
testcmd dkms && dkms status | cut -d' ' -f-4 | sed '1i\	DKMS status:'
separator
date -r /boot/grub/grub.cfg 2>$quiet | cut -d'+' -f1 | sed 's/^/grub.cfg last refreshed:	/g' &
grep -s '^GRUB_CMDLINE_LINUX_DEFAULT' /etc/default/grub 2>$quiet | cut -d'"' -f2 | sed 's/^/Cmdline - Grub:	/g' &
awk -F'"' 'NR==1{print $4}' /boot/refind_linux.conf 2>$quiet | sed 's/^/Cmdline - rEFInd:	/g' &
cut -d' ' -f4- /proc/cmdline | sed 's/^/Cmdline - booted:	/g')
#"
" -> Back" )
}
title='Kernel tools'; while menu_OK; do menudelay=5 sel kmenu $szl
case $opt in
	*\ modules\ *)
	mkdir -p /etc/modules-load.d; createarray <<< " -> Load a kernel module
 # Currently loaded, select to unload:
$(separator)
$(while read -r m; do mod=$(cut -d' ' -f1 <<<$m); echo "$mod [$(numfmt -z --to=iec --suffix='B]' "$(cut -d' ' -f2 <<<$m)"; grep -Rq "$mod" /etc/modules-load.d && echo -n ' [Loads at boot]')" & done </proc/modules | sort -k3r)
$(separator)
 -> Back"
	sel $szl; if [[ $opt == *Load* ]]; then
	read -rp "Type name of module or a wildcard (in which case the first module in alphabetical order will be listed): " qtl
	tl=$(find /lib/modules/"$(uname -r)" -type f -exec basename -a {} + | grep -o "$qtl.*.ko" | sed 's/\.ko$//g' | sort | head -n1); [[ -z $tl ]] && continue
	menu=( " # To be loaded: $tl" "Add this module on boot" "Load now" "Cancel" ); sel $szs
	case $opt in Add*) echo "$tl" >> /etc/modules-load.d/utilities_added.conf;; Load*) modprobe "$tl" && echo "Loaded $tl"; esac
	elif ! [[ -z $opt ]]; then modprobe -r "$(cut -d' ' -f1 <<<"$opt")"; else continue; fi
	;;
	*parameters)
	createarray <<<"$(sysctl -a)
 -> Back"
	header="Select one to set a new value$([ -t 0 ] && echo -n ', search using the hotkey (Ins.)')" sel $szl; if [[ $opt == *=* ]]; then
	text="Current value is:$(cut -d'=' -f2 <<<"$opt"). Input the new value:"; if [ -t 0 ]; then read -rp "$text " pval; else pval=$(gui_textentry); fi
	[[ -n $pval ]] && npval="$(cut -d'=' -f1 <<<"$opt" | tr -d ' ')=$pval" && su -c "sysctl -w $npval"
	else continue; fi
	;;
	*\ \-\>\ Run*|*\ \-\>\ Re*)
	kdtxt() {
		case $opt in
		*Grub\ conf*) grub-mkconfig -o /boot/grub/grub.cfg;;
		*Grub\ inst*) if [[ -d /sys/firmware/efi ]]; then grub-install || grub-install --target=x86_64-efi --efi-directory=esp_mount --bootloader-id=grub; else grub-install "$(df /boot | grep -o '/dev/sd[a-z]')"; fi;;
		*rEFInd*) refind-install --yes;;
		*depmod\ *) cd /lib/modules && find . -mindepth 1 -maxdepth 1 -type d -printf '%f\n' | while read -r k; do [[ $(du -sk "$k" | cut -f1) -lt 100 ]] && rm -rf "$k" && continue; echo "$k"; dkms autoinstall --no-depmod -v "$k" && depmod "$k" && echo OK; done; mkinitcpio -P;;
		*kmods\ *) rm -vf /etc/modules-load.d/utilities_added.conf;;
		esac
		}; if [ -t 0 ]; then kdtxt; else kdtxt 2>&1 | gui_textbox $szs; fi
	;;
	\#*) continue;;
	*Back|"") returntomain
esac
termslp; done
;;

-hwp*)
scmd() { if testcmd column; then column; else cat; fi; }
enterpstate() { echo manual > power_dpm_force_performance_level; if [ -t 0 ]; then read -rp 'Enter core frequency in MHz: ' gpuf && read -rp 'Enter core voltage in mV (e.g. 1050mv = 1.05v): ' gpuv; else gpuf=$(text='Enter core frequency in MHz' gui_textentry) && gpuv=$(text='Enter core voltage in mV (e.g. 1050mv = 1.05v)' gui_textentry); fi; input_OK $gpuf$gpuv; }
hwmenu() { sep=$(separator /2) header="$(testcmd python && grep -q 'Ryzen [3-9] [1-5]' /proc/cpuinfo && python <<<"import struct, os
def readmsr(msr, cpu = 0):
        f = os.open('/dev/cpu/%d/msr' % cpu, os.O_RDONLY)
        os.lseek(f, msr, os.SEEK_SET)
        return struct.unpack('Q', os.read(f, 8))[0]
def pstate2str(val):
    if val & (1 << 63):
        fid = val & 0xff
        did = (val & 0x3f00) >> 8
        return 'AMD PState: %.2f GHz' % (25*fid/(12.5 * did)/10)
print(pstate2str(readmsr(0xC0010064)))" 2>$quiet &
for c in /sys/devices/system/cpu/cpu*/cpufreq/scaling_cur*; do nr=$(tr -dc '0-9' <<<$c) nrg=$(cat "$(dirname "$c")"/scaling_governor); awk -v CONVFMT='%.2f' '{print "CPU'$nr': "sprintf( int($1) / 1000000 )" GHz ['$nrg']" }' <"$c" & done | sort | scmd | sed -e 's/^.*[a-z]/#	&/g; 1i\Current CPU frequency and governor:')"
createarray <<<"$(
{
find /sys/devices/system/cpu/cpufreq -name '*avail*governors' -exec cat {} + | tr ' ' '\n' | sed -e '/^$/d; s/^/ \-\> CPU governor select: /g' &
for b in /sys/class/backlight/*/brightness; do [[ $b == *\** ]] && continue; echo " -> Change display brightness for $(cut -d/ -f5 <<<$b)"; done &
for s in /sys/block/*/queue/scheduler; do sed -e "s/^/ \-\> I\/O Scheduler for drive $(cut -d/ -f4 <<<$s): /g; /: none$/d" <$s & done
} 2>$quiet | sort -u
)
$sep
$(
! [[ $gpuinfo == *AMD* ]] && return
sed 's/\]//g; s/^.*$/# [GPU: &]/g' <<<$gpuinfo &
grep -o '^\s[0-9] *[0-9A-Z_*]*' pp_power* | sed -e 's/\://g; 1i# Select profile:' &
[[ $(wc -c <"$pwrf"_max) -ge 9 ]] && echo " -> Set power limit [ curr. $(( $(<$pwrf) / 1000000 ))W ] (max. $(( $(<"$pwrf"_max) / 1000000 ))W)"
! grep -Rq '0xfff..fff' /proc/cmdline /etc/modprobe.d && echo -e "[!! Most functions need boot parameter 'amdgpu.ppfeaturemask=0xfffd7fff']\n$sep" && return
echo -ne "\n# Customize amdgpu overdrive cfg. [ mode:"
[[ $gpuinfo == *Navi* ]] &&
echo " Curves ]
 -> Edit middle point in freq./voltage curve [ curr. $(grep OD_VDDC_CURVE -A3 -a pp_od_clk_voltage | awk 'NR==3{print $2,$3}') ] (lowest allowed $(grep RANGE -A1 -a pp_od_clk_voltage | awk 'NR==2{print $2}'))
 -> Edit max/overclock point in freq./voltage curve [ curr. $(grep OD_VDDC_CURVE -A3 -a pp_od_clk_voltage | awk 'NR==4{print $2,$3}') ] (highest allowed $(grep RANGE -A1 -a pp_od_clk_voltage | awk 'NR==2{print $3}'))" ||
echo " PStates ]
 -> Limit GPU frequency to PState $(awk '/3:/ {print $1,$2,$3}' < pp_od_clk_voltage)
 -> Limit GPU frequency to PState $(awk '/4:/ {print $1,$2,$3}' < pp_od_clk_voltage)
 -> Limit GPU frequency to PState $(awk '/5:/ {print $1,$2,$3}' < pp_od_clk_voltage)
 -> Limit GPU frequency to PState $(awk '/6:/ {print $1,$2,$3}' < pp_od_clk_voltage)
 -> Let GPU use highest PState $(awk '/7:/ {print $1,$2,$3}' < pp_od_clk_voltage)
 -> Specify overclock freq/voltage on highest PState"
echo " -> Reset changes made to the table
$sep"
)
$(getsensors | sed 's/^.*[a-z]/	&/g' | scmd)
 -> Go to Fan control
 -> Back"
}
title='H/W Parameters'; while menu_OK; do gpustats info && [[ $gpuinfo == *AMD* ]] && pwrf=$(realpath ./hwmon/*/power1_cap); sel hwmenu $szl
case $opt in
	\ [0-9]\ *) cut -d' ' -f2 <<<$opt > pp_power*;;
	*\ 3:*) seq -s' ' 0 3 > pp_dpm_sclk;;
	*\ 4:*) seq -s' ' 0 4 > pp_dpm_sclk;;
	*\ 5:*) seq -s' ' 0 5 > pp_dpm_sclk;;
	*\ 6:*) seq -s' ' 0 6 > pp_dpm_sclk;;
	*\ 7:*) seq -s' ' 0 7 > pp_dpm_sclk;;
	*\ governor\ *) find /sys/devices/system/cpu/cpufreq -name 'scaling_governor' | while read -r c; do cut -d' ' -f6 <<<"$opt" > "$c"; done;;
	*\ drive\ [a-z]*) sf="/sys/block/$(cut -d' ' -f7 <<<$opt | tr -d ':')/queue/scheduler" numtotal=$(wc -w <"$sf") numcurr=$(sed 's/\].*//' <"$sf" | wc -w); [ "$numcurr" = "$numtotal" ] && numcurr=0; tr -d '][' <"$sf" | cut -d' ' -f $(( $numcurr + 1 )) > "$sf";;
	*limit*) text='Enter new power limit in W: '; if [ -t 0 ]; then read -rp "$text" gpup; else gpup=$(gui_textentry); fi; input_OK "$gpup" && echo $(( "$gpup" * 1000000 )) > $pwrf
	;;
	*Specify*) enterpstate && ( echo "s 7 $gpuf $gpuv" && echo c ) > pp_od_clk_voltage;;
	*middle*curve*) enterpstate && ( echo "vc 1 $gpuf $gpuv" && echo c ) > pp_od_clk_voltage;;
	*max*curve*) enterpstate && ( echo vc 2 $gpuf $gpuv && echo c ) > pp_od_clk_voltage;;
	*Reset*) echo r > pp_od_clk_voltage;;
	*control) main_return=false args_rootonly -fancontrol;;
	*brightness\ *) f="/sys/class/backlight/$(cut -d' ' -f7 <<<"$opt")"; if [ -t 0 ]; then read -rp "Enter value in %%: " val; else val=$(text="Choose value in %" val=50 min=1 max=100 gui_slider); fi; awk -v val="$val" '{printf int(val/100*$1)}' < "$f"/max_brightness > "$f"/brightness;;
	\#*|\	*) continue;;
	*) exit 0
esac
done
;;

-maint*)
checktermgui $1 && return
xdeskt=$($runfromroot xdg-user-dir DESKTOP || echo $userhome/Desktop) xdowns=$($runfromroot xdg-user-dir DOWNLOAD || echo $userhome/Downloads)
xdocs=$($runfromroot xdg-user-dir DOCUMENTS || echo $userhome/Documents) xpicts=$($runfromroot xdg-user-dir PICTURES || echo $userhome/Pictures)
exclusionlist() {
[[ $grb == *$xdeskt* ]] || [[ $grb == *$xdowns* ]] || [[ $grb == *$xdocs* ]] || [[ $grb == *$xpicts* ]] ||
[[ $grb == */Steam/* ]] || [[ $grb == */flatpak/repo/* ]]
}
while menu_OK; do
menu=(
' -> Quick cleanup'
' -> Full cleanup (close important programs first!)'
' -> Run fstrim on system drives'
' -> Run fstrim on all mounted drives'
' -> Defragment a volume'
' -> Back'
)
title='Maintenance'; header="Quick cleanup covers:\n- typical garbage files in target user's home directory (most programs do not need closing).\nFull cleanup adds:$(testcmd bleachbit && echo -n '\n- running bleachbit;')\n- extra browser/webview junk without touching passwords/bookmarks/most storage;\n- empty files/directories and broken symlinks in user's home directory;\n- parts of /var/{log,cache,tmp} not in use, a small selection of files in /etc and /opt.\n$generictext" sel
case $opt in
	*cleanup*)
	[[ $opt == *Full* ]] && if testcmd bleachbit; then echo -e 'Running bleachbit...\n'; mv -f /tmp/*auth* "$DIR"; [[ -f /root/.config/bleachbit/bleachbit.ini ]] && bleachbit --preset -c || bleachbit --all-but-warning -c; if [[ -z $nouser ]]; then [[ -f $userhome/.config/bleachbit/bleachbit.ini ]] && $runfromroot bleachbit --preset -c || $runfromroot bleachbit --all-but-warning -c; fi; mv "$DIR"/*auth* /tmp; fi 2>$quiet
	echo -e 'Running universal cleaner, should be 99% safe, press Ctrl+C to abort at any time...\n'
	( if [[ -z $nouser ]]; then
	# directories
	find "$userhome" -maxdepth 12 -type d \( -name '__pycache__' -o -path '*/strawberry/albumcovers' -o -path '*/share/Trash' -o -path '*/klipper' \
	-o -iregex '.*/cache\($\|\-.*\|_.*\|[0-9]*\|storage\|s\|d\)' -o -name '*[a-z ]Cache' -o -iname 'gvfs-metadata' -o -name 'RecentDocuments' \
	-o -iregex '.*/\(\-\|_\|\.\|gpu.*\|script.*\|media.*\|html\)cache' -o -regex '.*/Crash\(pad\| Reports\)' \)
	# files/symlinks
	find "$userhome" -maxdepth 8 -not -type d \( -iregex '.*\.\(bak\|tmp\|dmp\|log\|old\|err\|cache\)\($\|\-.*\|_.*\|\.[0-9]*\)' \
	-o -iname 'resourcecache*' -o -iname '*history\.[tdx][xam][tl]' -o -iname '*cache.dat' -o -path '*/mpv/watch*' -not -name 'prev*' \
	-o -path '*/xnview*' -not -name '*.ini' -o -name '*-bak\.*' -o -iname '*log.txt' -o -iname 'thumb*.db' -o -name 'queue.[0-9]' \
	-o -name '*lesshst' -o -name '*[\._]history' -o -name 'nohup.out' -o -name '*-shm' -o -name '*-wal' -o -name 'recently-used.xbel' \) \
	-not -name 'mime*' &
	# any
	find "$userhome" \( -iregex '.*/\(temp\|tmp\|dmp\|err\|log\|logs\)\($\|\-.*\|_.*\)' -o -iregex '.*\(\-\|_\)\(temp\|tmp\|dmp\|err\|log\|logs\)' \)
	if [[ $opt == *Full* ]]; then	# extra browser/webview
	profilepaths=$(find "$userhome" -maxdepth 5 \( -name 'cookies.sqlite' -o -name 'WebStorage' \) -printf '%h\n')
	[[ -n $profilepaths ]] && for i in $profilepaths; do find "$i" \( -iname 'session*' -o -iname '*history.*' -o -name 'Visited Links' -o -name '.org.chromium.Chromium.*' \
	-o -iname 'service*worker.*' -o -iname 'quotamanager*' -o -name 'Network Persistent State' -o -iname 'datareporting' -o -name 'bookmarkbackups' \
	-o -iname 'notificationstore.json' -o -name 'webappsstore.sqlite' -o -iname 'telemetry.*' -o -iname '*stats' -o -name 'times.json' -o -iname 'formhistory.*' \
	-o -iname 'thumbnails' -o -name '*Service*.txt' -o -iname 'to-be-removed' -o -iname 'appcache' -o -iname 'crashes' \); done
	# empty dirs
	find "$userhome" -type d -empty -not -iregex '.*\(\.d\|keep.*\)'
	# empty files
	find "$userhome" -type f -empty -not -iregex '.*\(lock.*\|portable.*\|media.*\|ignore\|placeholder.*\|keep.*\|py.*\|rc\)' &
	# broken links
	find "$userhome" -maxdepth 8 -xtype l
	fi; fi
	# /etc; /opt; /var
	find '/etc' '/opt' -type f \( -name '*.bak' -o -name '*.log' -o -name '*.tmp' -o -iname '*.dmp' -o -iname '*.old' -o -iname '*.pacnew' -o -iname '*.apk-new' \)
	inuse=$(ls -l /proc/[0-9]*/{fd,map_files}); find {log,cache,tmp} -type f | while read -r file; do [[ "$inuse" == *$file* ]] || echo "$file" & done
	) 2>$quiet |
	while read -r grb; do
	sz=$(du -sb "$grb" 2>$quiet | cut -f1)
	[[ -z $sz ]] || exclusionlist && continue
	if [[ $sz -lt 1000 ]]; then slp=0 #<1k/empty
	elif [[ $sz -lt 150000 ]]; then slp=0.2 #<150k
	elif [[ $sz -lt 2000000 ]]; then slp=0.8 #<2mb
	elif [[ $sz -lt 100000000 ]]; then slp=2 #<100mb
	else slp=10; fi #larger
	echo -n "Removing $(numfmt --to=iec --suffix=B "$sz") $grb" && sleep "$slp" && rm -rf "$grb" && printf ' *\n'; done; termslp
	;;
	*Defragment*)
	while menu_OK; do
	defrmenu() { createarray <<<"$(lsblk -rnfp | grep -E 'ext4|xfs|btrfs')
Back"; }
	title='Defragment a volume' header='Select a volume. ext4, xfs and btrfs volumes are supported' sel defrmenu
	[ "$opt" = 'Back' ] && break; fs=$(cut -d' ' -f1 <<<"$opt"); case $(lsblk -nfp "$fs" -oFSTYPE) in ext4) e4defrag "$fs";; xfs) xfs_fsr -v "$fs";; btrfs) btrfs filesystem defragment -rvf "$fs";; *) echo 'Unsupported volume'; esac; termslp
	done
	;;
	*fstrim*system*) echo 'In progress...'; fstrim -Av; termslp
	;;
	*fstrim*all*) echo 'In progress...'; fstrim -av; termslp
	;;
	*) returntomain
esac
done
;;

-mnt)
gethdir() { l=$(lsblk -ino LABEL "$hpart"); [[ -n "$l" ]] && hdir="/mnt/$l" || hdir="/mnt/$(basename "$hpart")"; dh=$(mount | grep $hdir); [[ -n $dh ]] && [[ ! $dh == *$hpart* ]] && hdir="/mnt/$(basename "$hpart")_$l"; ! [[ -d "$hdir" ]] && mkdir -p "$hdir" && [[ -z "$nouser" ]] && chown -R "$user_tgt" "$hdir"; }
if [ "$2" = automount ]; then partitions | while read -r hpart; do mount | grep -q $hpart && continue; gethdir; mount "$([[ -n "$3" ]] && echo -n "-o $3")" "$hpart" "$hdir" && writelog "Mounted $hpart to $hdir"; done; exit; fi
[[ -n $(ls -A '/mnt') ]] && find '/mnt' -maxdepth 1 -type d -empty -exec rmdir {} \; # clean any leftover dirs from previous mounts
while menu_OK; do
hdmenu() { createarray <<<"
$(if [[ -n "$(partitions)" ]]; then echo "[ Available to re/mount ]
$(partitions | while read -r i; do echo "$i$(lsblk -nr $i -oSIZE | head -n1 | awk -v usg="$(df $i --output=pcent | tr -dc '0-9')" '{ if (int(usg) > 0) print " ["$1", "usg"% used]"; else print " ["$1"]"}')$(mount | grep -q "$i" && mount | grep -o "$i.*(r[wo]" | head -n1 | sed -e 's/.*on / [Mounted as /; s/ type .*/] [&]/; s/ (/] [/; s/ type //' || echo ' [Not mounted]')"; done)"
else echo '[ No partitions found ]'; fi
[[ -n "$(lsblk -nfp /dev/sr0 -oFSTYPE 2>$quiet)" ]] && echo -n '/dev/sr0 [Optical media inserted]' && mount | grep -q '/dev/sr0' && echo -n ' [Mounted]')
$(separator; if mount | grep -q -E '\.iso|\.img|/dev/nbd'; then echo -e "[ Virtual volumes, select to unmount ]\n$(mount | grep -E '\.iso|\.img|/dev/nbd' | sed -e 's/.*on //' -e 's/ type.*//')"; separator; else echo '[ No virtual volumes mounted ]'; fi; [[ -n "$merr" ]] && echo -e "\n[$merr]")
 -> Mount image file (iso/img/qcow2)
 -> Main menu
 -> Exit"
}
title='Mount tools' header="Select a volume. If it has a custom LABEL, the mount directory will be named after it:" menudelay=1.5 sel hdmenu $szi
merr=; hpart=$(sed 's/ \[.*\]//' <<<"$opt")
[[ "$hd" == *nvme* ]] && hd=$(sed 's/p[0-9 ]*$//' <<<$hpart) || hd=$(sed 's/[0-9 ]*$//' <<<$hpart)

case $opt in
	' -> Mount image file (iso/img/qcow2)')
	text='Select iso/img/qcow2 file:' file_browser
	! [[ -s "$opt_file" ]] && continue
	mntdir="/mnt/$(basename "$opt_file" | tr -dc '[:alnum:] ._-')" &&
	if [[ "$opt_file" == *.img ]]; then
	di=$(fdisk -l -o Start,End "$opt_file") && sed -e '1,/Start/ d' <<<$di | while read -r i; do s=$(awk '{print $1}' <<<$i) e=$(awk '{print $2}' <<<$i) && mkdir -p "$mntdir"_"$s" && mount -o loop,offset=$(( "$s" * 512 )),sizelimit=$(( "$e" * 512 )) "$opt_file" "$mntdir"_"$s" || merr="Mounting .img at sector $s failed, probably missing or unsupported file system"; done
	elif [[ "$opt_file" == *.iso ]]; then
	mkdir -p "$mntdir"; mount -o loop "$opt_file" "$mntdir" || merr="Mounting .iso failed"
	elif [[ "$opt_file" == *.qcow2 ]]; then
	mkdir -p "$mntdir"; modprobe nbd max_part=4 && qemu-nbd --connect=/dev/nbd0 "$opt_file" && sleep .5 && mount /dev/nbd0p1 "$mntdir" || merr="Mounting .qcow2 failed"
	else false; fi || { [[ -n $merr ]] && rmdir "$mntdir" || merr="Invalid file"; }; [ ! -t 0 ] && [[ -n $mntdir ]] && $runfromroot xdg-open "$mntdir" & disown
	;;
	/dev*)
	gethdir; if [ "$hpart" = '/dev/sr0' ]; then if mount | grep -q "$hpart"; then umount $hpart && rmdir $hdir; else mount "$hpart" "$hdir"; fi
	else
	lsmod | grep -q fuse || modprobe fuse
	curhdir=$(lsblk "$hpart" -ino MOUNTPOINT) fs=$(lsblk -f "$hpart" -o fstype | sed '/^FS*/d; /^$/{q1}' || mount | grep $hpart | cut -d' ' -f5) title=$hpart hinfo=$(testcmd hdparm && hdparm -CI $hd 2>$quiet | grep -E 'Number:|Rot.*Rate: [1-9][0-9]|Factor:|level:|state is:') partinfo=$(testcmd tune2fs && tune2fs -l $hpart 2>$quiet | grep -E 'name|mounted|revision|state|created|checked') header=$([[ "$hinfo" == *Model* ]] && echo "$hinfo" && separator; [[ -n "$partinfo" ]] && echo $partinfo; [[ -n $curhdir ]] && echo "Mount point: $curhdir" && usg=$(fuser -m $curhdir 2>$quiet) && [[ -n "$usg" ]] && echo "Processes currently using the volume: $(xargs -r -P 2 ps -o comm= <<<$usg | tr '\n' ' ')")
	menu=(
		"(Re)mount as read only"
		"(Re)mount as rw"
		"Open file manager at the mount point"
		"$(mount | grep -q "$hpart" && echo -n 'Unmount this partition')"
		"$(mount | grep -q "$hd" && [[ $(lsblk "$hd" | grep -cE 'part.{2,}') -gt 1 ]] && echo -n 'Unmount every partition of the drive')"
		"$(testcmd udisksctl && echo -n 'Power off the drive')"
		"$(testcmd hdparm && hdparm -i "$hd" >$quiet 2>&1 && echo -n 'Put the drive in sleep state')"
		"Fix file system"
		" -> Back"
	)
	sel $szs
	case $opt in
		"(Re)mount as read only")
		if mount | grep -q "$hpart"; then mount -o remount,ro,noatime "$hpart" || ( umount "$hpart" && mount -o ro,noatime "$hpart" "$hdir" ); else mount -o ro,noatime "$hpart" "$hdir"; fi;;
		"(Re)mount as rw")
		[[ $fs =~ fuseblk|vfat|exfat ]] && mopts="noatime,uid=$(getuid $user_tgt),gid=$(getent passwd "$user_tgt" | cut -d: -f4)" || mopts='noatime'; if mount | grep -q "$hpart"; then mount -o remount,rw,$mopts "$hpart" || ( umount $hpart && mount -o $mopts "$hpart" "$hdir" ); else mount -o $mopts "$hpart" "$hdir"; fi
		;;
		"Unmount this partition")
		umount "$hpart" && if [[ -n "$curhdir" ]]; then rmdir "$curhdir"; else rmdir "$hdir"; fi;;
		"Unmount every partition of the drive")
		for i in "$hd"*; do umount "$i"; done;;
		"Power off the drive")
		mount | grep -q "$hd" && if ( [ -t 0 ] && read -rp 'There are still partitions mounted on the drive. Unmount them? (Y/n) ' qo && [[ "$qo" =~ y|Y|^$ ]] ) || ( [ ! -t 0 ] && text='There are still partitions mounted on the drive. Unmount them?' gui_question ); then for i in "$hd"*; do umount $i && rmdir "$hdir"; done; sleep .5; fi
		udisksctl power-off -b "$hd"
		;;
		"Put the drive in sleep state")
		hdparm -Y "$hd";;
		"Open file manager at the mount point")
		[ -t 0 ] && location="$hdir" fm_mode=1 file_browser || $runfromroot xdg-open "$hdir" & disown;;
		Fix*)
		case $fs in xfs) rcmd='xfs_repair';; btrfs) rcmd='btrfs check --repair';; vfat) rcmd='dosfsck -wavt';; *) rcmd='fsck -M'; esac
		if [ -t 0 ]; then $rcmd "$hpart"; termslp; else $rcmd "$hpart" 2>&1 | gui_textbox $szs; fi
	esac
	fi
	;;
	/mnt*|/run*) umount "$hpart" && rmdir "$hdir"; [ "$hpart" = '/mnt/qcow' ] && qemu-nbd --disconnect /dev/nbd0 && rmmod nbd
	;;
	*Main\ *) returntomain;;
	*Exit|"") exit 130
esac
done
;;

-usb)
while menu_OK; do
cd /sys/bus/usb/drivers/usb || exit 1
usbmenu() { createarray <<< "[Refresh]
 -> Force restart the USB subsystems (this also reconnects any disabled/suspended devices)
$(find -L . -maxdepth 2 -type f -name product 2>$quiet | while read -r dev; do grep -qE 'Controller|Hub' $dev && continue; echo "$(basename "$(dirname $dev)") [ $(cat $dev) ] [ USB$(head -c4 "$(dirname $dev)"/version) ] [ $(cat "$(dirname $dev)"/bMaxPower) ]" & done | sort)
 -> Back"; }
title='USB device manager'; header='Select a USB device to manage:' sel usbmenu $szi
case $opt in
	*Force\ *) for xhci in /sys/bus/pci/drivers/*hci_hcd; do cd $xhci && find -L . -maxdepth 1 -name "[0-9]*" -printf "%f\n" | while read -r sdev; do echo -n "$sdev" > unbind; sleep .5; echo -n "$sdev" > bind; done; done; sleep 3
	;;
	[0-9]*)
	header=$(cut -d' ' -f2- <<<"$opt") dev=$(cut -d' ' -f1 <<<"$opt"); while menu_OK; do
	menu=(
	"[ Device status: $([ "$(cat ./$dev/authorized)" = 1 ] && echo Online || echo Offline) ]"
	"Unbind/suspend (keep this menu open to reconnect later)"
	"Reconnect"
	"Remove/disable"
	" -> Back"
	)
	sel $szs; case $opt in
	Unbind*) echo -n "$dev" > unbind;;
	Reconnect) echo -n "$dev" > bind;;
	Remove*) echo -n "$dev" > ./$dev/remove; break;;
	*) break
	esac
	done
	;;
	*Refresh*) continue;;
	*Back|"") break
esac
done
;;

-srv)
[[ -z $init ]] && echo No alternative initsystem found && exit
autoex() { [[ "$1" =~ wpa_supplicant|iwd|hostapd ]] && rfkill unblock wifi; } # niche cases to run a useful command for some services
resolvesrv() { srv=$(sed -e 's/ \[[A-Z].*\]*//g; s/\[.*\] //g; s/\[ .*\]//g; s/(.*)//g; s/ [0-9].*$//g; s/\=user.*\= //g; s/\s//g' <<<"$opt"); } # extract service name from listings
acmd() { sed "/^$/d; s/^/[$text$(isimportant $srv && echo -n '*')] /g"; }; scmd() { acmd; }; servlist() {
case $init in
	OpenRC)
	startedserv=$(COLUMNS=$(getcols) rc-status -qqa | grep started | sed -E 's/\[  /\[ /g; s/\[.*[a-z]  \]//g; s/\s*$//g; s/^\s//g; s/ {9}//')
	availserv=$(rc-service -l)
	runsrv() { if ! [[ "$startedserv" == *$1* ]]; then rc-service $1 start; elif isimportant $1; then rc-service $1 restart; else rc-service $1 stop; fi; sleep .8; rc-service $1 status; termslp; }
	enabled=$(rc-update -a | sed -E 's/^\s{0,}//g; s/\|/[ /g; s/\s{1,}/ /g; s/\s{0,}$/ ]/g')
	addsrv() { createarray <<<"$(find /etc/runlevels/* -type d -exec basename -a {} +; grep -qE 'Available|\*\]' <<<"$opt" || echo "Remove from runlevel $(cut -d' ' -f4 <<<"$opt")")"; header="[$srv] Choose runlevel. If unsure pick \"default\"." sel $szs; case $opt in Remove*) rc-update del "$srv" "$(cut -d' ' -f4 <<<"$opt")";; *) rc-update add "$srv" "$opt"; esac; }
	;;
	Dinit)
	startedserv=$(dinitctl list | grep -vE '\[\s{0,}.(-|X)\s{0,}.\]')
	availserv=$(find /etc/dinit.d /usr/lib/dinit.d -type f -not \( -name '*.*' -o -path '*/user*' \) -exec basename -a {} + | sort -u)
	if [[ -d /run/turnstiled ]]; then
	usersvc_dctl() { dinitctl -u -d /run/turnstiled/"$uid"/srv."$(pgrep -n dinit -U "$uid")" -p /run/user/$uid/dinitctl "$@"; }
	find_usersvc() { getuid | while read -r uid; do ps aux | grep -o '..[s]ervices-dir /run/turnstiled/'$uid'.*' | sed 's/--services-dir //g' | tr ' ' '\n' | while read -r i; do [[ -d $i ]] && find "$i" -not -type d -path "*$([ "$1" = boot ] && echo -n 'boot.d')*" & done | if [ "$1" = boot ]; then syssvc=false getdeps; else xargs -r -P 2 basename -a; fi | sed "s/^/=user service for $(getuser $uid)= /g"; done; }
	resolveuser() { getuid "$(grep -oE 'user service for [[:alnum:]]{2,}' <<<"$opt" | cut -d' ' -f4 | tr -d \-)"; }
	started_usersvc=$(find /run/turnstiled -maxdepth 1 -name '1[0-9]*' -printf '%f\n' | while read -r uid; do usersvc_dctl list | grep -vE '\[\s{0,}.(-|X)\s{0,}.\]' | sed "s/ ]/& =user service for $(getuser $uid)=/g" & done) && startedserv+=$'\n'"$started_usersvc"''
	availserv+=$(find_usersvc | sort -u)
	fi
	runsrv() { if [[ "$opt" == *user\ serv* ]]; then dctl='usersvc_dctl' uid=$(resolveuser); srvrunning() { [[ $started_usersvc == *$1* ]]; }; else dctl='dinitctl'; srvrunning() { [[ $startedserv == *$1* ]]; }; fi; if ! srvrunning $1; then $dctl --no-wait start $1; elif isimportant $1; then $dctl restart $1; else $dctl --no-wait stop $1; fi; sleep .8; $dctl status "$1"; termslp; }
	getdeps() { while read -r s; do ! [[ -f $s ]] && echo $s && return; [[ "$s" == *lib* ]] && echo -ne "\n(internal) $(basename $s)" || echo -ne "\n$(basename $s)"; grep -oE '(waits|depends).*=.*' $s | cut -d= -f2 | sort -u | tr '\n' ' ' | sed -e 's/^ //; s/ $//; s/^.*$/   (Dep.-> &)/g' | tr -d '\n'
	${syssvc:?} && grep -oRE '(waits|depends).*=.*'"$(basename $s)"'\s{0,}$' /etc/dinit.d /usr/lib/dinit.d 2>$quiet | cut -d: -f1 | sort -u | sed -e '/\/user\//d; s/.*\///g' | tr '\n' ' ' | sed -e 's/^ //; s/ $//; s/^.*$/   (Dep.<- &)/g' | tr -d '\n'; echo -ne '\n'; done
	} && scmd() { syssvc=true getdeps | acmd; }
	enabled=$(type find_usersvc >$quiet 2>&1 && find_usersvc boot; find /etc/dinit.d/boot.d /usr/lib/dinit.d/boot.d -type l -not -xtype l 2>$quiet; grep -R internal /usr/lib/dinit.d | cut -d: -f1)
	addsrv() { if [[ "$opt" == *user\ serv* ]]; then dctl='usersvc_dctl' uid=$(resolveuser); else dctl='dinitctl'; fi; if grep -qE 'Available|\*\]' <<<"$opt"; then $dctl enable "$srv"; else $dctl disable "$srv"; fi; termslp; }
	;;
	runit)
	startedserv=$(sv status /etc/runit/sv/* | grep '(pid' | cut -d: -f2,3 | cut -d/ -f5 | tr -d \:)
	availserv=$(find /etc/runit/sv -mindepth 1 -maxdepth 1 -exec basename -a {} +;)
	enabled=$(find /etc/runit/runsvdir -mindepth 2 -type l -exec basename -a {} +;)
	runsrv() { if [[ ! "$startedserv" == *$1* ]]; then sv up $1; elif isimportant "$1"; then sv restart "$1"; else sv down "$1"; fi; termslp; }
	addsrv() { if grep -qE 'Available|\*\]' <<<"$opt"; then ln -s /etc/runit/sv/$srv /etc/runit/runsvdir/default; else rm /etc/runit/runsvdir/default/$srv; fi; }
	;;
	s6-linux-init)
	startedserv=$(s6-rc -a list)
	availserv=$(s6-rc-db list services)
	enabled=$(s6-rc-db contents default)
	runsrv() { if [[ ! "$startedserv" == *$1* ]]; then s6-rc -u change $1; elif isimporant $1; then s6-svc -r /run/service/$1; else s6-rc -d change $1; fi; }
esac
hidden="$(grep -E 'boot|system|root|localmount|killprocs|mount-ro|cgroups|acpid|sysfs|procfs|devfs' <<<$availserv)" # services always hidden
isimportant() { grep -qiE 'net\.|connmand|networkmanager|dbus|wpa_supplicant|iwd|dhcpcd|hostapd|cronie|swap|fuse|fsck|tty[0-9]|loopback|tigervnc|sshd|libvirtd|netmount|nfs|http|ftp|seatd|logind|turnstiled|gdm|xdm|lightdm|sddm|lxdm|greetd' <<<$1; } # important services marked with *
srvnum="Currently running: $(wc -l <<<"$startedserv"), enabled: $(wc -l <<<"$enabled")"
if [ "$ulist" = running ]; then
header="Select a service to be started or stopped, depending on it's status (* - only restartable).\n$srvnum"
createarray <<<"
$(type addsrv >$quiet 2>&1 && echo " -> Manage $init startup"
separator /2
while read -r srv; do [[ ! "$hidden" == *$srv* ]] && if [[ "$startedserv" == *$srv* ]]; then grep "$srv" <<<"$startedserv" | text=Running acmd; else echo "[Stopped] $srv"; fi & done <<<$availserv | sort -u
separator /2)
 -> Show [$(wc -l <<<$hidden)] hidden
 -> Main menu
 -> Exit"
elif [ "$ulist" = startup ]; then
header="Add or remove services from the startup list, depending on it's status (* - can only be enabled).\n$srvnum"
createarray <<<"$(
for srv in $availserv; do [[ "$hidden" == *$srv* ]] && continue; if [[ "$enabled" == *$srv* ]]; then grep "$srv" <<<"$enabled" | text=Enabled scmd; else text='\/\/Available' scmd <<<$srv; fi & done | sort -t'/' -k2 | uniq)
 -> Back"
fi
}
while menu_OK; do
title="Service manager [$init]"; ulist=running sel servlist $szi; case $opt in
	\[*) resolvesrv; [[ $availserv == *$srv* ]] && ! [[ $hidden == *$srv* ]] && if [ -t 0 ]; then runsrv "$srv" && autoex "$srv"; else runsrv "$srv" 2>&1 | gui_textbox $szs && autoex "$srv"; fi
	;;
	*hidden) hlist() { while read -r srv; do if [[ "$startedserv" == *$srv* ]]; then grep "$srv" <<<"$startedserv" | sed 's/^/[Running] /'; else echo "[Stopped] $srv"; fi; done <<<"$hidden" | sort -u; }; if [ -t 0 ]; then hlist; termslp; else hlist 2>&1 | gui_textbox $szs; fi
	;;
	*Manage*startup)
	title="Startup options [$init]"; while menu_OK; do
	ulist=startup sel servlist $szs; resolvesrv; case $opt in *\ Back|"") break;; *) if ! [ -t 0 ]; then addsrv 2>&1 | gui_textbox $szs; else addsrv; fi; esac
	done
	;;
	*Exit) exit 130;;
	*Main*|"") returntomain
esac
done
;;

-fw)
# check if nftables works
if ! nft list chain inet utilities_fw INPUT >$quiet 2>&1; then
echo -ne 'add table inet utilities_fw\n add chain inet utilities_fw INPUT { type filter hook input priority 0 ; }' | nft -f - || return 1; fi
# validate ip addresses with support for netmask at the end
ipv4_validate() {
grep -oE '((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])(/[1-3][0-9]|\s|$)'
}
ipv6_validate() {
grep -oE '(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))(/[1-3][0-9]{1,2}|\s|$)'
}
# ip filter components
ipfiltercount() { { nft list set inet utilities_fw ipmaster & nft list set inet utilities_fw ipmaster6; } | grep -oc ','; }
ipfilterapplied() { [[ "$(ipfiltercount 2>$quiet)" -gt 2 ]]; }
ipfilterstats() { echo "$(perl -E '/packets (\d+)/ and $s += $1 for `nft list chain inet utilities_fw INPUT_filter`; say $s || die' 2>$quiet || echo 0) hits since first apply (ranges: $(ipfiltercount))"; }
ipfilterprepare() {
if ipfilterapplied; then echo -n 'Flushing existing lists... '; nft flush set inet utilities_fw ipmaster && nft flush set inet utilities_fw ipmaster6 && ipcleaned=1; [[ -z $ipcleaned ]] && echo '!! IPSets not cleared successfully' && return 1 || echo -n 'OK; '; if [ -t 0 ]; then echo -ne '\nDo you want to rerun? [y/N] '; read -r -t30 ihs; [[ ! $ihs =~ y|Y ]] && return 1; else [[ -f /tmp/ip_lists ]] && rm /tmp/ip_lists; fi; fi
[[ -s /tmp/ip_lists ]] && [[ $( (( "$(cut -d. -f1 < /proc/uptime)" - "$(head -n1 < /tmp/ip_lists)" )) ) -lt 3600 ]] && echo -n "Recent IP blocklist file found, restoring..." && return 0
until ip route | grep -q 'default via.*dev'; do [[ $i = 180 ]] && writelog 'No network' && exit 1; sleep 10; let i=i+1; done # wait for a network
echo -n 'Fetching ip blocklists...'
( cut -d'.' -f1 < /proc/uptime
if [[ -f "$DIR/custom_ips" ]]; then lsattr "$DIR/custom_ips" | grep -q '\-i' || chattr +i "$DIR/custom_ips"; cat < "$DIR/custom_ips"; fi # append the custom_ips file and set immutable attr.
sed 's/#.*$//g' <<SRC_IPS | xargs curl -Z --parallel-max 2 --retry 10 -s | sort -u
# sources outputting any uncompressed format go below:
#
https://iplists.firehol.org/files/firehol_level{1,2}.netset
https://myip.ms/files/blacklist/general/latest_blacklist.txt
https://www.blocklist.de/downloads/export-ips_all.txt
https://www.spamhaus.org/drop/dropv6.txt
#
SRC_IPS
) | sed -E 's/(#|\;|\s).*$//g; 2,1000000s/^.{1,5}$//g; /^$/d' > /tmp/ip_lists # filter and output
}
ipfilterprocess() {
if ! nft list chain inet utilities_fw INPUT_filter >$quiet 2>&1; then
echo -ne 'add chain inet utilities_fw INPUT_filter { type filter hook input priority 1 ; }
add set inet utilities_fw ipmaster { flags interval; type ipv4_addr; auto-merge; }
add set inet utilities_fw whitelist { flags interval; type ipv4_addr; auto-merge; }
add set inet utilities_fw ipmaster6 { flags interval; type ipv6_addr; auto-merge; }
add set inet utilities_fw whitelist6 { flags interval; type ipv6_addr; auto-merge; }
insert rule inet utilities_fw INPUT_filter ip saddr @ipmaster counter drop
insert rule inet utilities_fw INPUT_filter ip saddr @whitelist accept
insert rule inet utilities_fw INPUT_filter ip6 saddr @ipmaster6 counter drop
insert rule inet utilities_fw INPUT_filter ip6 saddr @whitelist6 accept' | nft -f - || return 1; fi
# prevent potential conflicts + import
echo -n 'add element inet utilities_fw whitelist {
10.0.0.0/8,
100.64.0.0/10,
169.254.0.0/16,
172.16.0.0/12,
192.168.0.0/16,
198.18.0.0/15,
255.255.255.255/32,
'"$({ ip addr; cat /etc/resolv.conf; } | ipv4_validate | sed 's/$/,/g')"'
}
add element inet utilities_fw whitelist6 {
::1/128,
64:ff9b::/96,
64:ff9b:1::/48,
100::/64,
2001:20::/28,
5f00::/16,
'"$({ ip addr; cat /etc/resolv.conf; } | ipv6_validate | sed 's/$/,/g')"'
}
add element inet utilities_fw ipmaster {
'"$(ipv4_validate </tmp/ip_lists | sed 's/$/,/g')"'
}
add element inet utilities_fw ipmaster6 {
'"$(ipv6_validate </tmp/ip_lists | sed 's/$/,/g')"'
}
' | nft -f - || { echo -n " !! IPSets not processed correctly"; [[ -f /tmp/ip_lists ]] && rm /tmp/ip_lists; }
echo -ne " $(ipfiltercount) ranges applied\n"
}
# wg components
wg_connect() {
[[ -n $wg_file ]] && file=$wg_file || file=$opt_file; ! [[ -s "$file" ]] && return 1; iname=$(basename -s .conf "$file") address=$(grep '^Address' "$file" | ipv4_validate) address6=$(grep '^Address' "$file" | ipv6_validate)
ip link add $iname type wireguard || return 1; wg setconf $iname <(grep -E '^\[Interface|^\[Peer|^PrivateKey|^PublicKey|^Endpoint|^AllowedIPs|^$' "$file") || return 1
ip -4 address add $address dev $iname; ip -6 address add $address6 dev $iname
grep '^DNS' "$file" | sed 's/DNS =/nameserver/' | resolvconf -a tun."$iname" -m 0 -x; wg set $iname fwmark 51820
ip link set mtu 1420 up dev $iname; sysctl -q net.ipv4.conf.all.src_valid_mark=1
ip -4 route add 0.0.0.0/0 dev $iname table 51820; ip -6 route add ::/0 dev $iname table 51820
for cmd in 'ip -4' 'ip -6'; do $cmd rule add not fwmark 51820 table 51820; $cmd rule add table main suppress_prefixlength 0; done
echo -n 'add chain inet utilities_fw preraw { type filter hook prerouting priority -300; }
add rule inet utilities_fw preraw iifname != '$iname' ip daddr '"$(cut -d/ -f1 <<<$address)"' fib saddr type != local drop
add rule inet utilities_fw preraw iifname != '$iname' ip6 daddr '"$(cut -d/ -f1 <<<$address6)"' fib saddr type != local drop
add chain inet utilities_fw premangle { type filter hook prerouting priority -150; }
add rule inet utilities_fw premangle meta l4proto udp meta mark set ct mark
add chain inet utilities_fw postmangle { type filter hook postrouting priority -150; }
add rule inet utilities_fw postmangle meta l4proto udp mark 51820 ct mark set mark' | nft -f - && writelog "$iname connected"
}
wg_disconnect() {
for cmd in 'ip -4' 'ip -6'; do $cmd rule delete table 51820; $cmd rule delete table main suppress_prefixlength 0; done
echo -ne 'flush chain inet utilities_fw preraw\n flush chain inet utilities_fw premangle\n flush chain inet utilities_fw postmangle' | nft -f -
resolvconf -d tun."$iname" -f; ip link delete dev $iname
}
# startup
if [[ -f $wg_file ]] || [[ $2 == *filter ]]; then [[ $2 == *filter ]] && ( ipfilterapplied && writelog "$(ipfilterstats)"; writelog "$(ipfilterprepare && ipfilterprocess)"; ); [[ -f $wg_file ]] && wg_connect; exit; fi
while menu_OK; do
wgif=$(testcmd wg && wg show interfaces | head -n1)
fwmenu() { testcmd ss && currp=$(ss -atupn)
createarray <<<"
$(if testcmd wg; then [[ -n $wgif ]] && separator /2 && echo -ne " -> Disconnect Wireguard connection $wgif\n -> Override DNS for Wireguard connection $wgif [current: $(ipv4_validate </etc/resolv.conf | tr '\n' ' ')]" || echo -n " -> Connect Wireguard .conf file"; fi)
$(separator /2)
 -> $(ipfilterapplied && echo -n "[$(ipfilterstats)] Reset" || echo -n 'Run') IP blocking filter
 -> Open a TCP port
 -> Open a UDP port
 -> Block an IPv4 address
 -> Block an IPv6 address
 -> Whitelist an IPv4 address
 -> Whitelist an IPv6 address
$({ pgrep -lf 'hostapd|x0vncserver|xrdp|X11vnc' | tr '\n' ' '; [[ -n $SSH_TTY ]] && echo -n 'conn. over SSH'; } | sed 's/^/!! No kill switch function available, running: /;/[a-z]/{q1}' && separator /2 && ls /sys/class/net | while read -r int; do echo " -> Kill switch for traffic on interface $int"; done
[[ -n $currp ]] && awk '{print $5,$7}'<<<$currp | grep -oE ':[0-9]{2,5}($| *| *users.*)' | sed -e 's/^:/ .:: Local port in use: /g; s/users[:("]*/by /g; s/".*//g; s/ *$//g; s/$/, select to limit/g' | sort -u && separator /2)
 -> Export config created by this menu
 -> Clear config created by this menu
 -> Import config
 -> Back"
header="Currently using: $(nft --version | cut -d' ' -f1,2)\n# Status: $({ nft list chain inet utilities_fw INPUT & nft list chain inet utilities_fw INPUT_filter; } | grep -cE '^\s{1,}i.*') rule(s) applied, $(perl -E '/packets (\d+)/ and $s += $1 for `nft list chain inet utilities_fw INPUT`; say $s; print " hits" || die' 2>$quiet || echo "0 hits"
[[ -n $currp ]] && echo -n '\n# Connection stats:' && ls -A /sys/class/net | while read -r if; do ip address show $if | sed -E 's/\/[0-9]{1,}//g' | while read -r ip; do grep -cE "$(ipv4_validate <<<$ip | tr '\n' '|' | head -c-1)" <<<$currp 2>$quiet & grep -cE "$(ipv6_validate <<<$ip | tr '\n' '|' | head -c-1)" <<<$currp 2>$quiet & done | awk '{n += $1}; END{printf " ['$if': "int(n)" active]"}'; done)"
}
title='Firewall manager' sel fwmenu
case $opt in
	*\ Connect*Wireguard*) text=$opt file_browser; wg_connect;;
	*\ Disconnect*Wireguard*) iname=$wgif wg_disconnect;;
	*\ Override*DNS*) read -rp 'Enter an IPv4 address here: ' pip; ip=$(ipv4_validate <<<"$pip"); [[ -z $ip ]] && continue; echo "nameserver $ip" | resolvconf -a tun."$iname" -m 0 -x;;
	*\ filter) ipfilterprepare && ipfilterprocess;;
	*\ Open*TCP*) read -rp 'Enter a TCP port here: ' port; nft -e insert rule inet utilities_fw INPUT tcp dport "$port" counter accept;;
	*\ Open*UDP*) read -rp 'Enter a UDP port here: ' port; nft -e insert rule inet utilities_fw INPUT udp dport "$port" counter accept;;
	*\ Whitelist*v4*) read -rp 'Enter an IPv4 address here: ' pip; ip=$(ipv4_validate <<<"$pip"); [[ -z $ip ]] && continue; nft -e insert rule inet utilities_fw INPUT ip saddr "$ip" counter accept;;
	*\ Whitelist*v6*) read -rp 'Enter an IPv6 address here: ' pip; ip=$(ipv6_validate <<<"$pip"); [[ -z $ip ]] && continue; nft -e insert rule inet utilities_fw INPUT ip6 saddr "$ip" counter accept;;
	*\ Block*v4*) read -rp 'Enter an IPv4 address here: ' pip; ip=$(ipv4_validate <<<"$pip"); [[ -z $ip ]] && continue; nft -e insert rule inet utilities_fw INPUT ip saddr "$ip" counter drop;;
	*\ Block*v6*) read -rp 'Enter an IPv6 address here: ' pip; ip=$(ipv6_validate <<<"$pip"); [[ -z $ip ]] && continue; nft -e insert rule inet utilities_fw INPUT ip6 saddr "$ip" counter drop;;
	*\ Kill*) echo 'Clear the config to reset'; nft -e insert rule inet utilities_fw INPUT iifname "$(awk '{ print $NF }' <<<"$opt")" drop;;
	*\ limit) port=$(tr -dc '0-9' <<<"$opt"); read -rp "Enter no. of connections to limit for port $port: " ct; for a in tcp udp; do nft -e insert rule inet utilities_fw INPUT $a dport "$port" meter p"$port"$a \{ ip saddr ct count over "$ct" \} counter reject; done;;
	*\ Export*) text=$opt file_browser; nft -j list table inet utilities_fw > "$opt_file" && continue;;
	*\ Import*) text=$opt file_browser; nft -f "$opt_file";;
	*\ Clear*) nft delete table inet utilities_fw;;
	\#*) continue;;
	*\ Back|"") returntomain
esac; sleep 1
done
;;

-fancont*)
getrpm() { cat "$(awk -F/ 'BEGIN { OFS = FS }; NF { NF -= 1 }; 1' <<<"$1")/fan${1: -1}_input" 2>$quiet; }
getspd() { awk -v CONVFMT='%.0f' '{print sprintf($1/2.55)}' < $1; }
fansfound() { find /sys/class/hwmon/*/ -regex '.*/*/pwm[0-9]' | while read -r i; do [[ $(getrpm $i) -gt 10 ]] && echo $i & done; }
while menu_OK; do
fanmenu() {
fanlist=$( { fansfound & tr ',' '\n' <<<$fans; } | sort -u ); fans2=$(tr '\n' ',' <<<$fanlist); [ "$fans" != "$fans2" ] && fans=$fans2 && writeconf
createarray <<<"
[Refresh]
$( [[ -z "$fanlist" ]] && echo 'No fans found' || while read -r i; do [[ ! -f $i ]] && continue; echo "$i$([[ -f $(dirname $i)/name ]] && echo " [$(head -c6 "$(dirname $i)"/name)]")$([[ $(cat "$i"_enable) = 2 ]] && echo ' [Auto]' || echo ' [Manual]')$([[ $(getspd $i) -le 1 ]] && echo " [Off]" || echo " [Currently at $(getspd $i)%, $(getrpm $i) RPM]")" & done <<<"$fanlist" | sort)
 -> Main menu
 -> Exit"; }
title='Fan control' header="The script detects fans that are running, saving them in the utconfig file in case they will be turned off later." sel fanmenu $szs
case $opt in
	*sys*)
	fan=$(cut -d' ' -f1 <<< "$opt")
	if [ -t 0 ]; then read -rp "Enter a speed in %: " val; else val=$(text="$([[ $(getspd $fan) -le 1 ]] && echo The fan is currently off || echo Current speed is "$(getspd $fan)"% at "$(getrpm $fan)" RPM). Choose a speed in %" val=50 min=0 max=100 gui_slider); fi; [[ -z "$val" ]] && continue
	echo -n 1 > "$fan"_enable && awk -v CONVFMT='%.0f' BEGIN'{print sprintf('$val'*2.55)}' > "$fan" || exit $?;;
	*Refresh*) continue;; *Main\ *) returntomain;; ""|*Exit) exit 130
esac; done
;;

# internal usage
-addrule) echo ''$user_tgt' ALL=(ALL) NOPASSWD: '$script_path', /bin/iotop*' > /etc/sudoers.d/utilities && chattr +i "$script_path" && writelog "Added rule for user $user_tgt";;
-rmrule) rm -f /etc/sudoers.d/utilities && chattr -i "$script_path" && writelog "Removed rule for user $user_tgt";;
-testrule) grep -sq "$script_path" /etc/sudoers.d/utilities && echo "Script has sudoers rule for user $(cut -d' ' -f1 </etc/sudoers.d/utilities)";;
-pkg*ls_root)
checktermgui $1 && return
pickb() { text='Specify path to text file containing the list of packages or c to choose: '; read -rp $text plf; [[ $plf = c ]] && file_browser && plf=$opt_file; list=$(awk '{print $1}' <$plf); }
[[ -f /var/lib/pacman/db.lck ]] && rm -iv /var/lib/pacman/db.lck
case $2 in
	*Install*)
	read -rp "Specify one or more names, a path to package file, or a search query. Type c to choose, b to bulk install. Leave empty to return: " pkgvar; [[ -z "$pkgvar" ]] && return
	if [[ -f "$pkgvar" ]]; then testcmd pacman && pacman -U "$pkgvar" 2>&1; testcmd apk && apk add --allow-untrusted "$pkgvar" 2>&1; testcmd opkg && opkg install --force-overwrite --force-reinstall --force-checksum "$pkgvar" 2>&1
	elif [ "$pkgvar" = c ]; then file_browser; testcmd pacman && pacman -U "$opt_file" 2>&1; testcmd apk && apk add --allow-untrusted "$opt_file" 2>&1; testcmd opkg && opkg install --force-overwrite --force-reinstall --force-checksum "$opt_file" 2>&1
	elif [ "$pkgvar" = b ]; then pickb; while read -r i; do testcmd pacman && pacman -S --needed --noconfirm "$i"; testcmd apk && yes | apk add "$i"; testcmd opkg && yes | opkg install "$i"; done <<<"$list"
	elif input_OK $pkgvar; then
	testcmd pacman && if [[ -z $(pacman -Si "$pkgvar") ]]; then if [[ -n $(pacman -S "$pkgvar" --groups) ]]; then pacman -S --needed $pkgvar; else echo "Package/s $pkgvar not found, searching..."; pacman -Sy >$quiet && pacman -Ss $pkgvar | less -K -O$quiet; fi; else pacman -S $pkgvar; fi; testcmd apk && apk add $pkgvar; testcmd opkg && opkg install $pkgvar
	else echo Invalid option; fi
	;;
	*Remove*)
	read -rp "Specify package(s) to remove. Type b for bulk uninstall$(testcmd pacman && echo -n ", o to remove [$(pacman -Qtdq | wc -l)] orphans"). Leave empty to return: " pkgvar; [[ -z "$pkgvar" ]] && return
	if [ "$pkgvar" = b ]; then pickb; while read -r i; do testcmd pacman && pacman -R --noconfirm "$i"; testcmd apk && apk del "$i"; testcmd opkg && opkg remove --autoremove "$i"; done <<<"$list"
	elif [ "$pkgvar" = o ] && testcmd pacman; then orp=$(pacman -Qtdq | tr '\n' ' '); if [[ -n $orp ]]; then echo -ne "Orphans found: $orp. Remove? (Y/n)"; read -r qo; [[ $qo =~ y|Y|^$ ]] && pacman -Qtdq | xargs pacman -Rns --noconfirm; else echo No orphans found; fi
	elif input_OK $pkgvar; then
	testcmd pacman && if grep -q ' ' <<<$pkgvar; then pacman -Rns $pkgvar; elif pacman -Qi $pkgvar | grep -q 'Reason.*dependency'; then echo -ne "Package $pkgvar is\n[ $(pacman -Qi "$pkgvar" | grep 'Required By') ]\nRemove forcefully? (y/N) "; read -r q; [[ $q =~ y|Y ]] && pacman -Rdd $pkgvar; else pacman -Rns $pkgvar; fi; testcmd apk && apk del $pkgvar; testcmd opkg && opkg remove --autoremove $pkgvar
	else echo Invalid option; fi
	;;
	*Update*) testcmd pacman && pacman -Syu --overwrite=\* 2>&1; testcmd apk && apk upgrade 2>&1; testcmd opkg && opkg update && opkg list-upgradable | cut -d' ' -f1 | xargs -r opkg upgrade --force-overwrite;;
	*Force*) testcmd pacman && pacman -Syyuu --overwrite=\* 2>&1; testcmd apk && apk -aUv upgrade 2>&1; testcmd opkg && opkg update && opkg list-upgradable | cut -d' ' -f1 | xargs -r opkg upgrade --force-overwrite --force-downgrade;;
	*Clean*) testcmd pacman && pacman -Scc; testcmd apk && apk cache clean -v; testcmd opkg && rm -rvf /tmp/opkg* /var/cache/opkg*;;
	*Audit*) testcmd pacman && find /etc /usr /opt | LC_ALL=C pacman -Qqo - 2>&1 >&- >$quiet | cut -d' ' -f5- | less -K -O$quiet; testcmd apk && apk audit 2>&1 | less -K -O$quiet; testcmd opkg && opkg list-changed-conffiles 2>&1;;
	*Fix*) apk fix 2>&1;;
	*Purge*) rm -rvf '/var/cache/pacman' '/var/lib/pacman/sync/';;
	*Change*) read -rp "Specify package to mark as explicitly installed: " exp; [[ -n $exp ]] && input_OK $exp && pacman -D --asexplicit $exp;;
	*Populate*) pacman-key --populate;;
	*Refresh*) pacman-key --refresh-keys;;
	*) exit
esac
termslp; main_return=false args_all -pkgutils
;;

*)
help_text; termslp
esac
}

################################################## User only arguments

args_useronly() {

if runningasroot; then $runfromroot "$script_path" "$1"; else

case $1 in

-wine)
[ -t 0 ] && echo 'This option is meant to run in the GUI mode' && sleep 2
wineprocs() { ps -aU $user_tgt | grep -E '\.ex$|\.exe$|wineserver|wine-|wine64|winedump' 2>$quiet; }; has32() { [[ -n "$(ls -A /usr/lib32)" ]]; }; has32 && wine=wine || wine=wine64; wmode=exe
prefix_detect() {
withinpref=$(cd "$(dirname "$exe")" && while [[ "$(realpath .)" != / ]]; do [[ -d drive_c ]] && realpath "$(dirname drive_c)" && break; cd ..; done)
if [[ -n $withinpref ]]; then prefix=$withinpref && wtext="Prefix detected: $withinpref"; elif [[ -n $prefix ]]; then wtext="Using specified prefix: $prefix"; else prefix="$userhome/.wine" && wtext="Using default prefix $userhome/.wine"; fi
if ! text="$wtext\nIs it OK[Yes], or specify a different one[No]?" gui_question; then prefix=$(text='Choose prefix directory' gui_filechooser --dir); fi
}
runwine() {
PATH="$winepath/bin:$PATH" LD_LIBRARY_PATH="$winepath/lib:$LD_LIBRARY_PATH" WINEDLLOVERRIDES="winemenubuilder.exe=d" WINESERVER="$winepath/bin/wineserver" WINELOADER="$winepath/bin/wine" WINEDLLPATH="$winepath/lib/wine" WINEPREFIX=$prefix DXVK_HUD=devinfo,fps,gpuload,version "$winepath/bin/$wine" "$@"
}
while menu_OK; do
pk=$(find $DIR -maxdepth 1 -type f -size +30M -name "wine*"); [[ -n "$pk" ]] && text="Packages found, extract?\n$pk" gui_question && while read -r f; do wdirn="$DIR/$(basename "$f" | sed -e 's/\.tar.*//; s/\.t.z$//')"; mkdir -p "$wdirn" && tar -xf "$f" -C "$wdirn" && rm "$f" & done <<<"$pk" # extract wine packages
header="$(has32 && printf '[ 32 and 64 bit support ]' || printf '[ 64bit only system (WoW64 builds have 32bit support) ]')\nSelect the Wine version to run. For custom versions, put next to this script a Wine package/custom build (e.g. TkG) to be extracted." title="Wine deploy" exe=
createarray <<<"
 -> Mode: Run .exe (default) $([[ $wmode = exe ]] && echo -n '*')
 -> Mode: Run winecfg $([[ $wmode = wcfg ]] && echo -n '*')
 -> Mode: Run control panel $([[ $wmode = cpl ]] && echo -n '*')
 -> Mode: Install dxvk in a prefix $([[ $wmode = dvk ]] && echo -n '*')
$(for f in $([[ -f /bin/$wine ]] && echo -n "/bin/$wine") $(find "$DIR" -mindepth 2 -maxdepth 5 -executable -type f -name "$wine"); do echo "$($f --version) In location: $(realpath $f)" & done | sort -k2 & wineprocs >$quiet && echo " -> Terminate any running wineserver and associated programs")"
sel $szs; case $opt in
	wine*[0-9]*)
	winepath=$(dirname "$(dirname "$(sed "s/.*In location: //g" <<<"$opt")")")
	case $wmode in
		wcfg) prefix_detect; runwine winecfg;;
		cpl) prefix_detect; runwine control;;
		exe) exe=$(text='Choose .exe file. This requires Z: to be mounted, default in standard wineprefix' gui_filechooser); [[ ! -s $exe ]] && unset exe && continue; prefix_detect; runwine "$exe" || return $?;;
		dvk)
		arc=$(text='Choose dxvk package' gui_filechooser) dvkb=$(basename "$arc" | sed 's/\.tar.*//'); prefix_detect
		( echo $dvkb; mkdir -p "/tmp/$dvkb"; tar -xf "$arc" -C "/tmp/$dvkb" && cd "/tmp/$dvkb"/dxvk*/ &&
		mv -vf ./x64/*.dll "$prefix/dosdevices/c:/windows/system32" &&
		mv -vf ./x32/*.dll "$prefix/dosdevices/c:/windows/syswow64" &&
		for d in dxgi d3d9 d3d10core d3d11; do runwine reg add 'HKEY_CURRENT_USER\Software\Wine\DllOverrides' /v "$d" /d native /f; done ) 2>&1 | gui_textbox $szs
	esac
	;;
	*\ Mode*dxvk*) wmode=dvk;;
	*\ Mode*exe*) wmode=exe;;
	*\ Mode*winecfg*) wmode=wcfg;;
	*\ Mode*panel*) wmode=cpl;;
	*\ Terminate*) testcmd wineserver && wineserver -w && sleep 1; wineprocs | xargs kill -SIGTERM; sleep 2; wineprocs | xargs kill -SIGKILL; sleep 1; true;;
	*) break
esac
done
;;

-disptools)
! on_x11 && {
testcmd wayland-info && {
choosedisp() { wayland-info | grep -A1 wl_output | awk 'END{print " -> Choose display: "$2}'; }
getdi() { di=$(wayland-info | grep -A10 "$wldisplay") curres=$(grep -o 'width.*Hz' <<<$di) curscale=$(awk '/logical_width|width.*px/{printf $2" "}' <<<$di | awk '{print int( int($2) * 100 / int($1) )"%"}'); }
}
testcmd wlr-randr && wlr-randr >$quiet 2>&1 && {
choosedisp() { wlr-randr | grep -B1 Make | awk 'NR==1{print " -> Choose display: "$1}'; }
getdi() { di=$(wlr-randr --output "$wldisplay") curres=$(awk '/current/{print $1,$2,int($3)"Hz"}' <<<$di) curscale=$(awk '/Scale:/{print ($2 * 100)"%"}' <<<$di); }
setres() { wlr-randr --output "$wldisplay" --mode "$res"@"$ref"; }
setscale() { wlr-randr --output "$wldisplay" --scale "$scale"; }
}
testcmd kanshi && {
setres() { pkill -U $user_tgt kanshi; echo -e "profile {\n   output $wldisplay mode $res@$ref\n}" | kanshi -c /dev/stdin & disown; }
setscale() { pkill -U $user_tgt kanshi; echo -e "profile {\n   output $wldisplay scale $scale\n}" | kanshi -c /dev/stdin & disown; }
}
testcmd kscreen-doctor && pgrep -U $user_tgt kwin_wayland && {
setres() { kscreen-doctor output."$wldisplay".mode."$res"@"$(cut -d. -f1<<<$ref)"; }
setscale() { kscreen-doctor output."$wldisplay".scale."$scale"; }
}
wldisplay=$(choosedisp | head -n1 | cut -d' ' -f5)
}
while menu_OK; do
[[ -n $wldisplay ]] && getdi || unset wldisplay
createarray <<<"$(
on_x11 && xprop -root >$quiet 2>&1 && {
testcmd xrandr && echo " -> Set display resolution [ Currently $(xrandr | awk '/*/{print $1,$2}') ]"
testcmd xset && echo " -> Set display timeout $(xset q 2>$quiet | grep -q Enabled && awk -v a="$(xset q | grep timeout | grep -oE '[0-9]{2,}\s')" BEGIN'{printf "[ Currently "( a / 60 )"m ]"}')"
testcmd x0vncserver && echo -n ' -> Start a VNC server on current session' && pgrep -U $user_tgt x0vncserver | sed 's/^.*$/ [ Running, PID & ]/'
testcmd xrandr && echo -e "\n -> Set TearFree mode [ Currently $(xrandr --verbose | grep TearFree | grep -oE 'on|off' || echo auto) ]"
pgrep icewmbg >$quiet && echo ' -> icewmbg - Randomize background'
}
! on_x11 && {
choosedisp | grep -v "$wldisplay" || echo ' -> Input display manually'
[[ -n $wldisplay ]] && echo -e " -> Set display resolution [ Currently: $curres ]\n -> Set display scale [ Currently $curscale ]"
testcmd wayvnc && echo -n ' -> Start a VNC server on current session' && pgrep -U "$user_tgt" wayvnc | sed 's/^.*$/ [ Running, PID & ]/'
}
)
# -> Go to Hardware - Parameters for more potential options
  -> Back"
header="MODE: $(on_x11 && echo X11 || echo "Wayland\nDisplay: $wldisplay"; [ -t 0 ] && echo '\n[!! Some options might require running the script within the desktop session] ')\n$generictext" title='Display tools'; sel $szs
case $opt in
	*Choose\ *) wldisplay=$(cut -d' ' -f5 <<<$opt);;
	*manually) wldisplay=$(text=$opt gui_textentry);;
	*Parameters\ *) run_rootonly -hwp;;
	*resolution\ *)
	createarray <<<"$({ on_x11 && xrandr; [[ -n $di ]] && echo $di && find -L /sys/class/drm -maxdepth 6 -name modes -exec cat {} \; 2>$quiet; } | grep -oE '[0-9]{3,4}x[0-9]{3,4}' | sort -u)"; header='Choose resolution:' sel $szl; if [[ -n $opt ]]; then res=$opt; createarray <<<"$({ xrandr; [[ -n $di ]] && echo $di; } 2>$quiet | { grep $res || echo '60.00'; } | grep -oE '[0-9]{2}\.[0-9]{2,}' | sort -u)"; header="Choose refresh rate for $res" sel $szl; ref=$(tr -dc '0-9.' <<<$opt); if on_x11; then [[ -n $ref ]] && xrandr -s "$res" -r "$ref" || xrandr -s "$res"; else setres; fi; fi
	;;
	*scale\ *)
	[ ! -t 0 ] && scale=$(min=100 max=300 val=100 text='Scale in %' gui_slider | awk '{printf ($1/100)}') || read -rp 'Scale factor: ' scale; [[ -n $scale ]] && setscale;;
	*timeout*)
	text='Choose standby time in minutes or 0 to turn the screen off immediately: '; [ -t 0 ] && read -rp "$text" scr || scr=$(max='240' min=0 val=15 title='Display standby' gui_slider)
	if [ "$scr" = 0 ]; then sleep 2 && xset dpms force off; elif [[ $scr -gt 0 ]]; then xset s $(( "$scr" * 60 )); else return; fi
	;;
	*VNC*)
	if on_x11; then
	[[ ! -f ~/.vnc/passwd ]] && text='Password file is missing, create it now?' gui_question && mkdir -p ~/.vnc && $term vncpasswd
	x0vncserver -passwordfile ~/.vnc/passwd -display :0 MaxProcessorUsage=75 CompareFB=1 PollingCycle=20 1>&2 & disown
	elif [[ ! -s $userhome/.config/wayvnc/config ]]; then text="No config, wayvnc will be started with enable_auth=true enable_pam=true for auth. with user's credentials" gui_warn && echo -e 'enable_auth=true\nenable_pam=true' | wayvnc 0.0.0.0 -vgC /dev/stdin 1>&2 & disown; else wayvnc 0.0.0.0 -vg 1>&2 & disown; fi
	;;
	*TearFree*) xrandr | grep ' connected' | cut -d' ' -f1 | while read -r dp; do xrandr --output "$dp" --set TearFree "$(xrandr --verbose | grep -q 'TearFree: on' && echo -n 'off' || echo -n 'on')" & done;;
	*icewmbg*) icewmbg -r;;
	*) break
esac
done
;;

-customcomm*)
ccmd() { $([[ -n $tmo ]] && echo -n "timeout $tmo") env userhome="$userhome" opt="$opt" sep="$sep" sh; }
[[ ! -s $DIR/custom_commands ]] && echo '
# Menu format:

# plaintext for raw commands, or:
# !titles
# @options
# %case command or equivalent, to run based on selected text ($opt); parsed for all options

# Example:

!Preset - mozilla profiles found

@$(for i in firefox firefox-esr floorp librewolf icecat firedragon palemoon seamonkey; do command -v "$i" >/dev/null 2>&1 && echo "Profile manager for $i" && find "$userhome" -maxdepth 5 -name 'cookies.sqlite' -printf "Run $i with profile %h\\n" & done | sort)

%case $opt in *Profile*) $(cut -d\  -f6 <<<"$opt") -no-remote -ProfileManager;;
%*profile*) $(cut -d\  -f4 <<<"$opt") -no-remote -profile $(cut -d\  -f7- <<<"$opt")
%esac
' > $DIR/custom_commands
while menu_OK; do sep=$(separator /3)
createarray <<<" -> [ Refresh ]
 -> [ Run directory: $PWD; select to change ]
$(
[[ -s $DIR/custom_commands ]] && dc=$(date -r $DIR/custom_commands | cut -d'+' -f1) &&
	sed -e "/^\#/d; /^\%/d; s/^[a-z0-9]/ \>Run\> &/g; s/^\!//g; s/^[a-zA-Z0-9].*$/ $sep \[ & \] $sep/g; s/^\@//g; s/^.*$/echo \"&\"/g" $DIR/custom_commands 2>$quiet | tmo=1 ccmd | sed -e "s/^[a-zA-Z0-9]/ \-\> &/g; 1i\# custom_commands last modified:	$dc"
	find $userhome -maxdepth 5 -type f -executable | while read -r i; do [[ "$(basename "$(dirname "$i")")" == *$(basename "$i")* ]] || [[ $i == [rR]un*\.sh ]] || [[ $i == *[sS]tart*\.sh ]] || [[ $i == *[lL]aunch*\.sh ]] || [[ $i == *\.[aA]pp*mage ]] || [[ $i == *\.run ]] && echo "$i" & done 2>$quiet | sed -e "s/^/ \>Run\> /g; 1i\ $sep [Launchers in your homedir] $sep"
	cat $userhome/.local/share/applications/*.desktop /usr/share/applications/*.desktop 2>$quiet | grep -iE 'exec=' | sed -E "s/([tT]ry[eE]|[eE])xec=//g; s/ -[a-zA-Z] %[fFuU].*$//g; s/ %[a-zA-Z].*$//g; s/^/ \>Run\> /g; 1i\ $sep [Parsed from .desktop entries] $sep" | uniq
)
 -> [ Main menu ]
"
title='Misc. tools' header="$generictext\n'custom_commands' file next to this script adds new entries, consult format inside"
[ ! -t 0 ] && header+="\nIn GUI mode the commands do not run interactively, error output goes to script's log"
sel $szl
case $opt in
*Refresh\ \]|\#*) continue;;
*change\ \]) file_browser --dir; cd "$opt_file" || return;;
*\[\ Main\ *|"") returntomain;;
*) if [[ $opt == \ \-\>\ * ]]; then grep '^%' $DIR/custom_commands | sed 's/^\%//g'; else sed -E 's/^.{7}//' <<<"$opt"; fi | if [ -t 0 ]; then ccmd 2>&1; else ccmd & disown; fi; termslp
esac
done
;;

*)
! $argcontinue && argmatched=false && return; help_text; termslp
esac; fi
}

################################################## Universal arguments, also redirects to rootonly/useronly when needed

args_all() {

case $1 in

-pkg*ls)
while menu_OK; do
! testcmd pacman && ! testcmd apk && ! testcmd opkg && echo 'No compatible package manager found' && exit
createarray <<<"$(testcmd pacman && ! runningasroot && for u in yay paru trizen pakku yaourt yaourtix; do testcmd $u && echo "  -> [$u] Use AUR utility to search/install/update"; done)
  -> Search/display info - Installed packages
  -> Search/display info - Available packages
# -> Install package(s)
# -> Remove package(s)
# -> Update packages
# -> Force update packages, allowing downgrades
# -> Audit file changes
# -> Clean caches
$(testcmd pacman && echo '
# -> Purge all caches
# -> Change a package to "explicitly installed"
# -> Populate keys
# -> Refresh keys'
testcmd apk && echo '
# -> Fix a former error
')
  -> Back"
title='Package mgmt. tools' header="$(testcmd pacman && echo "Package stats: explicitly installed $(pacman -Qqe | wc -l), total $(( "$(ls -A /var/lib/pacman/local/ | wc -l)" - 1 ))"; testcmd apk && apk stats 2>$quiet | grep packages -m1 | sed 's/^\s/Stats: /g'; testcmd opkg && opkg list-installed | wc -l | sed 's/^/Installed: /g')\n$generictext" sel $szs
nopkgwarn() { if [ -t 0 ]; then echo 'No package found'; termslp; else text='No package found' gui_warn; fi; }
case $opt in
	\#*)
	$(runningasroot && echo -n 'args_rootonly' || echo -n "$([ -t 0 ] || echo -n $term) $sub $script_path") -pkgutils_root "$(cut -d' ' -f3 <<<"$opt")"; return $?;;
	*AUR*)
	autil=$(cut -d']' -f1 <<<"$opt" | cut -d'[' -f2); [ ! -t 0 ] && autil_inst="$term $autil" || autil_inst=$autil
	text='Specify package to search for, or leave empty for update mode: ' header="Select package to install:" title=$autil
	if [ -t 0 ]; then read -rp "$text" pvar; else pvar=$(gui_textentry); fi
	if [[ -z "$pvar" ]]; then $autil_inst -Sau; termslp
	elif input_OK $pvar; then pkg=$($autil -Ss "$pvar" | sed -z 's/\n    / [/g' | sed 's/$/]/'); if [[ -z "$pkg" ]]; then nopkgwarn && continue || break; else createarray <<<"$pkg"; sel $szi; [[ -z $opt ]] && return 0; [ "$autil" = paru ] && autil_inst+=" --sudoloop"; $autil_inst -S "$(cut -d' ' -f 1 <<<"$opt")"; termslp; fi; fi
	;;
	*Installed*)
	text="Enter search query/regex or leave empty to display all$(testcmd pacman && echo -e '.\nUtils:\n-Type F to show foreign/AUR packages;\n-Type E to show all explicitly installed;\n-Type S to show all packages without dependants incl. simple orphans\n'): " header='Select a package to display details and size(s)' title='Search inst. pkgs.'
	pkg_filter() { if [ "$src" = F ]; then pacman -Qm; elif [ "$src" = E ]; then pacman -Qe; elif [ "$src" = S ]; then pacman -Qi | grep -B19  '^Required By.*None$' | awk '/Name/{print $3}'; else testcmd pacman && pacman -Q | grep -iE "$src"; testcmd apk && apk info | grep -iE "$src"; testcmd opkg && opkg list-installed | grep -iE "$src"; fi; }
	pkg_info() { [[ -n $opt ]] && if testcmd pacman; then pacman -Qi "$(cut -d' ' -f1 <<<"$opt")" && echo Files used by package && pacman -Qlq "$(cut -d' ' -f1 <<<"$opt")" | while read -r f; do [[ -d "$f" ]] && continue; du -h "$f" 2>$quiet & done | sort -rh; elif testcmd apk; then apk policy "$opt"; apk info -a "$opt"; elif testcmd opkg; then opkg info "$opt"; fi | less -K -O$quiet; }
	if [ -t 0 ]; then read -rp "$text" src; else src=$(gui_textentry) || break; fi
	if [[ -n $(pkg_filter) ]]; then createarray <<<"$(pkg_filter)" && sel $szi && if [ -t 0 ]; then [[ -n $opt ]] && pkg_info; termslp; else [[ -n $opt ]] && pkg_info | gui_textbox $szi; fi; else nopkgwarn && continue || break; fi
	;;
	*Available*)
	text='Enter search query: ' header='Select a package to display details' title='Search avail. pkgs.'
	pkg_filter() { testcmd pacman && pacman -Ss "$src" | sed -z 's/\n    / [/g' | sed 's/$/]/'; testcmd apk && apk search "$src" | sed 's/-[0-9].*//g'; testcmd opkg && opkg find "$src" | grep '-'; }
	pkg_info() { [[ -n $opt ]] && if testcmd pacman; then pacman -Si "$(cut -d' ' -f1 <<<"$opt")"; elif testcmd apk; then apk info $opt; elif testcmd opkg; then opkg info "$opt"; fi; }
	if [ -t 0 ]; then read -rp "$text" src; else src=$(gui_textentry) || break; fi
	if [[ -n "$(pkg_filter)" ]]; then createarray <<<"$(pkg_filter)" && sel $szi && if [ -t 0 ]; then [[ -n "$opt" ]] && pkg_info; termslp; else [[ -n "$opt" ]] && pkg_info | gui_textbox $szi; fi; else nopkgwarn && continue || break; fi
	;;
	*Back*|"") returntomain
esac
done
;;

-prefs)
pgrep -U $user_tgt -f '(zenity|kdialog).*[t]itle.{,2}Preferences' && writelog 'Another instance is already running' && exit # avoid having multiple instances of settings open
shortcutfile="$userhome/.local/share/applications/utilities.desktop" deskt_text="[Desktop Entry]\nEncoding=UTF-8\nName=Utilities\nComment=Utilities script\nExec=$script_path\nType=Application\nCategories=System" # location and text for the shortcut
title='Preferences' header='Select on the options to toggle them, they can also be changed in the utconfig file located next to this script'
while menu_OK; do
suppterminals='alacritty kgx kitty konsole lxterminal qterminal terminator tilix xfce4-terminal'
getst() {
createarray <<<"$(! runningasroot && echo '	Terminal emulator of choice:' && for t in $suppterminals; do echo -e "~$t$(testcmd "$t" && echo -n ' [Installed]' || echo -n ' [Not installed]'; [[ -n "$terminal" ]] && [ "$t" = "$terminal" ] && echo -n ' [ * ]')"; done && separator /3)
	Show optional packages: $(! $showopts && echo No || echo Yes)
	Console - autorefresh: $(! $autorefresh && echo No || echo Yes)
	Console - scrollback (disabling also turns off quick screen refresh, use in case of issues): $(! $scrollback && echo No || echo Yes)
	Use GUI mode where possible: $(! $use_gui && echo No || echo Yes)
$([ -t 0 ] && echo -e "	-> Input RGB for custom theme [current: $([[ -z $customtheme ]] && echo -n 'default]' || echo -n "$customtheme]")\n	-> Or enter hex codes here"
has_sudoers_attr && echo " -> Remove the sudoers rule and allow the script to be modified" || echo "	Ask to add the sudoers rule: $(! $sudoers && echo No || echo Yes)"
! runningasroot && if [[ -f $shortcutfile ]]; then echo " -> Remove the shortcut placed in your programs folder"; ! diff -q <(echo -e $deskt_text) <(cat $shortcutfile) >$quiet && echo -e "$deskt_text" > "$shortcutfile" && writelog '[Shortcut mismatched, was replaced]'; else echo " -> Add a shortcut to this script in your programs folder"; fi)
 -> Show help and debug info
 -> Main menu
 -> Exit"
}
sel getst $szs
case $opt in
	\~*) trmp=$(cut -d'~' -f2 <<<"$opt" | awk '{print $1}'); grep -q "$trmp" <<<"$suppterminals" && terminal=$trmp && unset tr;;
	*optional*) [[ "$opt" == *Yes* ]] && showopts=false || showopts=true;;
	*Autorefresh*) [[ "$opt" == *Yes* ]] && autorefresh=false || autorefresh=true;;
	*Scrollback*) [[ "$opt" == *Yes* ]] && scrollback=false || scrollback=true;;
	*GUI*) ! [[ "$opt" == *Yes* ]] && use_gui=true && unset cmdonly || use_gui=false;;
	*RGB*) [[ -z $customtheme ]] && ctt="$color1+$color2" || ctt=$customtheme; read -rp 'Format: R;G;B+R;G;B: ' ctt; customtheme=$(tr -dc '0-9;+' <<<$ctt);;
	*hex*) read -rp 'Format: #background+#foreground: ' hex; customtheme=$( printf "%d;%d;%d+%d;%d;%d" 0x${hex:1:2} 0x${hex:3:2} 0x${hex:5:2} 0x${hex:9:2} 0x${hex:11:2} 0x${hex:13:2} );;
	*Remove*sudoers*) sudo "$script_path" -rmrule; unset sudoers;;
	*Ask*sudoers*) [[ "$opt" == *Yes* ]] && sudoers=false || sudoers=true;;
	*Add*shortcut*) echo -e "$deskt_text" > "$shortcutfile";;
	*Remove*shortcut*) rm "$shortcutfile";;
	*help*) if [ -t 0 ]; then help_text; termslp; else help_text | gui_textbox $szs; fi;;
	*Main*|"") writeconf; returntomain;;
	*Exit) writeconf; return 130
esac
done
;;

-audiomgr)
while menu_OK; do sep=$(separator /2)
audiomenu() { createarray <<<"$(adevs | while read -r s; do echo "$( { dirname $s | cut -d'/' -f4,5 && grep '^name' "$(dirname "$h")"/info 2>$quiet && grep -vE '^access|^subformat' $s; } | sed -e 's/^.*$/[&]/g; s/ (.*)//g' | tr '\n' ' ' )"; done | sed '1i\# ALSA devices active:'
! runningasroot && pgrep -U $user_tgt pipewire >$quiet 2>&1 && echo -e "# Pipewire streams active:\n$sep\n -> Change main rate to 44.1KHz\n -> Change main rate to 48KHz\n -> Change main rate to 96KHz" &&
( pw-top -b -n2 | awk '/R/{if ($12) print $2". [Format: "$10,$11,$12" Lat."$5"]"; else if ( $5 ~ /[0-9]/ ) print $2". "}'; wpctl status | sed -e '1,/Sinks/ d' | grep -vE ' >| <| monitor_' | grep -oE '[0-9]{2,3}\. ' ) | sort -k1,1 -u | while read -r s; do sn=$(cut -d' ' -f1 <<<$s); echo "$s $( ( wpctl inspect $sn 2>$quiet | grep -E 'media.(class|role|icon-name) |(application|client).name |quality |client.api |nick |alsa.(id|card) |rate ' | sed '/default/{q1}' && wpctl get-volume $sn 2>$quiet | awk -v CONVFMT='%.0f' '{print $1,($2*100)"%"}' ) | sed -E 's/^\s{1,}//g; s/ \= /: /g; s/\* //g; s/\"//g; s/.*(id|nick|-name)/name/g; s/(device|client|application|resample|node|media)\.//g; s/^.*$/[&]/g' | sort -k1,1 -u | tr '\n' ' ')" & done | sort -n
)
$sep
 -> Back"; }
title='Audio manager' header="Select an option for volume control:" sel audiomenu $szi
case $opt in
	\[card[0-9]*) $([ -t 0 ] || echo -n $term) alsamixer -c "$(tr -dc '0-9' <<<"$opt" | head -c1)";;
	[0-9]*) str=$(cut -d' ' -f1 <<<"$opt"); text="Volume for stream [$str]"; [ ! -t 0 ] && vol=$(val=50 min=0 max=100 gui_slider) || read -rp "$text in %: " vol; [[ -n $vol ]] && wpctl set-volume $str "$vol"% || return $?;;
	*44.1KHz) pw-metadata -n settings 0 clock.force-rate 44100;; *48KHz) pw-metadata -n settings 0 clock.force-rate 48000;; *96KHz) pw-metadata -n settings 0 clock.force-rate 96000;;
	*Back|"") return
esac
done
;;

-arc)
checktermgui $1 && return
while menu_OK; do
if [[ -f "$DIR"/7zz ]]; then util="$DIR"/7zz; elif testcmd 7zz; then util=7zz; elif testcmd 7z; then util=7z; else writelog 'No 7zip binary found, place 7zz binary next to script or install a package'; termslp; break; fi
menu=(
' -> Pick an existing archive file'
' -> Create new archive (full - multi source)'
' -> Create new archive (quick - single source)'
" -> (quick) Home directory backup for user $user_tgt"
' -> Back'
)
[[ -z "$opt_file" ]] && title='Archiving & backup tools' header="7zip binary: $(which $util) (version $($util i | grep -oEm1 '[0-9][0-9]\.[0-9][0-9]'))\nSupported formats:\n* For packing/adding to existing archive: .7z, .zip\n* For packing only(backups): .tar.bz2 .tar.gz .tar.xz [Uses 7zip binary as compressor] [Tar exclusion lists supported]\n* For unpacking: Anything suppored by the 7zip and tar binaries" sel && case $opt in
	*existing*) text=$opt file_browser;;
	*new\ *full*) text='Choose destination first:' file_browser --save;;
	*new\ *quick*) text="Choose source:" file_browser --dir; opt_dir="$opt_file"; opt_file=; text="Choose destination file:" file_browser --save;;
	*backup*) text='Create new .tar.bz2/gz/xz file:' file_browser --save; if [[ -n "$opt_file" ]]; then opt_dir="$userhome" skip_questions=1; fi;;
	*Back|"") break
esac

extract() {
extr_file="$opt_file" extr_dir="$opt_dir"; reset; if [[ "$extr_file" == *.t[bxga][zr]* ]]; then read -rp "Add an exclusion list on extraction? (y/N) " qbe && [[ $qbe =~ y|Y ]] && text='Choose tar exclusion list' file_browser; [[ -f "$opt_file" ]] && xxcl="$opt_file" || xxcl='/dev/null'; else read -rp "List the contents of \"$extr_file\" first? (y/N) " qbe && [[ $qbe =~ y|Y ]] && $util l "$extr_file" && termslp; fi
[[ -z "$extr_dir" ]] && text='Choose location to extract to:' file_browser --dir && extr_dir=$opt_file; [[ ! -d "$extr_dir" ]] && return; [ "$extr_dir" = "$userhome" ] && read -rp "Extract to user home? This will overwrite any existing files (y/N) " qbe && [[ ! $qbe =~ y|Y ]] && return
if [[ -n $xxcl ]]; then tar -X "$xxcl" -xvf "$extr_file" -C "$extr_dir"; else $util x "$extr_file" -o"$extr_dir"; fi
}
reset() { unset opt_file opt_dir skip_questions; }
invtxt() { echo 'Invalid option specified'; reset; }

if [[ "$opt_file" == *.7z ]] || [[ "$opt_file" == *.zip ]]; then

if [[ -s "$opt_file" ]]; then read -rp 'Extract [e] or add to existing archive [a]?' qz; elif [[ -s $opt_dir ]]; then echo "$opt_dir" > /tmp/arclist.txt; elif [[ -d $(dirname "$opt_file") ]]; then qz=a; else continue; fi
case $qz in a)
text='Type path for the file/directory to archive. Press m for multiple selection in GUI, c to choose using file selector, Enter to return: '
while true; do
if [[ -z "$tba" ]]; then read -rp "$text" tba; else clear; [[ -n "$arc" ]] && read -rp "$(echo -e "Currently added:\n$(IFS='#=#'; for i in $arc; do [[ -z "$i" ]] && continue; echo $i; done)\n\nAdd more entries or press Enter to finish\n\n")" tba || read -rp "$text" tba; fi
if [[ -e "$tba" ]] || [[ -d "$tba" ]]; then arc+="#=#$tba"; elif [ "$tba" = m ]; then tba="$(zenity --file-selection --filename="$userhome/." --title='Choose a file/directory' --multiple --separator='#=#' 2>$quiet)" && arc+="#=#$tba"; elif [ "$tba" = c ]; then file_browser; arc+="#=#$opt_file"; elif [[ -z "$tba" ]]; then break; else echo "File missing or invalid option specified, try again"; sleep 1; fi
done
clear; [[ -z $arc ]] && echo 'Nothing to add' && termslp && continue
( IFS='#=#'; for i in $arc; do [[ -z "$i" ]] && continue; echo "$i" | sed 's/^/"/;s/$/"/'; done ) > /tmp/arclist.txt # Create listfile for 7z to avoid path errors
;; e) extract;; "");; *) invtxt; continue; esac
echo -e "To be archived:\n$(cat /tmp/arclist.txt)\nArchive file:\n$opt_file\nStart? [10s] (Y/n)"; read -r -t10 q
[[ $q =~ y|Y|^$ ]] && $util a -bb3 "$opt_file" @/tmp/arclist.txt

elif [[ "$opt_file" == *.t[bxga][zr]* ]]; then

if [[ -s "$opt_file" ]]; then extract; elif [[ -d $(dirname "$opt_file") ]]; then
[[ -z $opt_dir ]] && read -rp 'Type path for the directory to archive. Press c to choose. For home directory backup press h: ' opt_dir
[ "$opt_dir" = c ] && forig="$opt_file" && file_browser --dir && opt_dir="$opt_file" && opt_file="$forig"; [ "$opt_dir" = h ] && opt_dir=$userhome; [[ -z "$opt_dir" ]] && reset && break
! cd $opt_dir && reset && return 1
if [[ -z $skip_questions ]]; then
! [[ "$(getent passwd)" == *$opt_dir* ]] && qbx=n || read -t10 -rp 'Include some system files in the backup? [10s] (Y/n) ' qbx
read -r -t5 -p "Add a timestamp to the archive? [5s] (Y/n) " qbt
[[ ! -f .excluded ]] && text=empty && [[ $qbx =~ y|Y|^$ ]] && text=default; grep -q 'delete##' .excluded 2>$quiet && text="manually saved on $(realpath .excluded)"; read -r -t10 -p "Would you like to review the exclusion list? ( $text ) [10s] (y/N) " qbe
fi
[[ $qbe =~ y|Y ]] && nano .excluded
! [[ $qbt =~ n|N ]] && opt_file=$(sed "s/\.t[bxga][zr]/-$(date '+%d_%b_%Y-%H.%M')&/" <<<"$opt"_file)
! [[ $qbx =~ n|N ]] && (
mkdir -p /tmp/bkr && cd $_ || return 1; echo -n 'Exporting system files of interest...'
cp -r --parents /etc/cron.d/ /etc/default/grub /etc/modprobe.d/ /etc/modules-load.d/ /etc/rc.local /etc/environment /etc/pacman.conf /etc/apk/ . 2>$quiet
testcmd pacman && { pacman -Qn | awk '{print $1}'; echo -e '\n# AUR packages:\n'; pacman -Qm | awk '{print $1}'; } > packages.txt && cp /etc/pacman.conf . && find /etc /usr /opt -not -path '*/ca-certificates/*' -size +100 2>$quiet | LC_ALL=C pacman -Qqo - 2>&1 >&- >$quiet | cut -d' ' -f5- | xargs cp -r --parents --target-directory=. 2>$quiet
testcmd apk && apk info > packages.txt && apk audit | grep -v '^D' | cut -d' ' -f2 | xargs cp -r --parents --target-directory=. 2>$quiet
cd "$userhome" && $util a -sdel "System files to restore.7z" /tmp/bkr/* >$quiet 2>&1 && echo -ne ' OK\n' && reset
[[ ! -f .excluded ]] && printf '*Downloads\nDesktop\nVideos\nPublic\n.cache\n.wine*\n.gnupg\n.local/share/Trash\n.gradle\n.cargo\n.npm\n.nvm\n.conan' > .excluded
)
[[ "$opt_file" == *.t*bz2 ]] && szf='-tbzip2'; [[ "$opt_file" == *.t*xz ]] && szf='-txz'; [[ "$opt_file" == *.t*gz ]] && szf='-tgzip'
tar --exclude-ignore=.excluded -cp . | $util a -si "$szf" "$opt_file" && reset
rm 'System files to restore.7z' 2>$quiet; if [[ -f .excluded ]] && ! grep -q 'delete##' .excluded; then read -rp 'Also remove the exclusion list (Y/n) [you might need it in the future] ' qbd; if [[ $qbd =~ y|Y|^$ ]]; then rm -v .excluded; else sed -i '1i\##Backup exclusion list, do not delete##' .excluded; fi; fi
else invtxt; fi

elif [[ -s "$opt_file" ]]; then extract
else invtxt; fi
reset; termslp; done
;;

-iomons)
checktermgui $1 && return; opt=; sopt=; stx=; ! runningasroot && has_sudoers_attr && sopt="-> Run as root for more info" stx='sudo'
ioperdisk() { sed -e '/sd[a-z][0-9] /d; /nvme.*p/d; /zram/d; s/^[0-9 ]*//g' /proc/diskstats; }; if testcmd column; then scmd() { column -c "$cols"; } else scmd() { cat; }; fi
iomons() { createarray <<<"$(
	getsensors | scmd &
	{ join /proc/net/dev <(sleep .5; cat /proc/net/dev) | awk '!/lo:/{if (int($26) > 0) print "[Network-int "$1" "(($18-$2)*2)" down, "(($26-$10)*2)" up]"}' &
	join <(ioperdisk) <(sleep .5; ioperdisk) | awk '{if (int($4) > 0) print "[Disk "$1": "(($21-$4)*512*2)" reads, "(($25-$8)*512*2)" writes]"}'
	} | sort | numfmt --field=3,5 --from=iec --to=iec --suffix='B/s' --padding=6 | scmd | sed '1i\[ I/O Summary ] ↓'
	ps -Ao pcpu,rss,comm,cmd 2>$quiet | sort -k1nr -k2nr | awk -v CONVFMT="%.1f" 'BEGIN{print "[ Top processes by CPU/RAM ] ↓"}; NR<15{print ($1/'$cpus')"%CPU",($2/1024)"M",$3,$4,$5}' | scmd &
	[[ -n $stx ]] || runningasroot && testcmd iotop && $stx iotop -obn1
	[[ -n $sopt ]] && echo $sopt
)
-> Back"; }
title='Sensors and I/O'; while menu_OK; do cols=$(getcols) header=$([[ -z $opt ]] && echo 'Selection acts as a highlight, go to the bottom to return:' || echo "Highlighted -> $opt") menudelay=0.75 force_aref=1 sel iomons; [[ $opt == *as\ root* ]] && sudo "$script_path" -iomons; [[ -z $opt ]] || [[ $opt == *\ Back* ]] && return; done
;;

-dltools)
inputlink() { text='Enter a link: '; [ -t 0 ] && read -rp "$text" link || link=$(gui_textentry); }; [ -t 0 ] && unset term tr
while menu_OK; do
[[ ! -d "$downdir" ]] && downdir="$($(runningasroot && echo -n $runfromroot) xdg-user-dir DOWNLOAD || echo $userhome/Downloads)"
menu=(
"Target directory: $downdir [ $(df -h "$downdir" | awk END'{print $4}') avail ]. Select to choose another"
"$(separator)"
" -> [httrack] Download/mirror websites"
" -> [wget] Download files listed on a website"
" -> [aria2] Download a file with multiple connctions"
" -> [aria2] Rip online stream(s) from m3u/pls playlist"
"$([ ! -t 0 ] && echo -n ' -> [ffmpeg] Rip online stream and transcode the audio')"
" -> [yt-dlp] Download a video or playlist"
" -> Main menu"
" -> Exit"
)
title='Downloaders'; header="$(for p in httrack curl wget aria2c ffmpeg yt-dlp; do testcmd $p || misg+="$p "; done; [[ -n $misg ]] && echo "[ ! These commands are missing: $misg ]" && unset misg)" sel $szs; case $opt in
	\!*) continue;;
	*directory*) text='Choose the download directory:' file_browser --dir; downdir="$opt_file";;
	*httrack*)
	inputlink; [[ -z "$link" ]] && continue
	wtitle=$(wget -T 1 -qO- "$link" | perl -l -0777 -ne 'print $1 if /<title.*?>\s*(.*?)\s*<\/title/si' | tr -d '></\"}|{^'); [[ -z "$wtitle" ]] && return 1
	mkdir -p "$downdir/$wtitle" && cd "$_" || return 1
	text='Select depth (2 for single page, 3 for pages with nested links/folders, 4+ for entire websites): '; [ -t 0 ] && read -rp "$text" depth || depth=$(val="2" min="2" max="7" title='Httrack' gui_slider); [[ -z "$depth" ]] && depth=2
	$term httrack -v -s0 --depth=$depth -n -A200000 --disable-security-limits -F 'Mozilla/5.0' "$link"
	;;
	*wget*)
	inputlink; [[ -z "$link" ]] && continue
	text='Specify type of file(s) to download (ex: jpg or jpg,png,zip or wildcards): '; [ -t 0 ] && read -rp "$text" typ || typ="$(gui_textentry)"
	text='Select recurse depth: '; [ -t 0 ] && read -rp "$text" depth || depth=$(val="2" min="1" max="4" title='Wget' gui_slider); [[ -z "$depth" ]] && depth=2
	cd "$downdir" && $term wget -x -N -T 1 -nH -np -e robots=off --user-agent='Mozilla/5.0' -A "$typ" -r -H -l "$depth" "$link"
	;;
	*multiple*)
	inputlink; [[ -z "$link" ]] && continue
	text='Select number of connections: '; [ -t 0 ] && read -rp "$text" maxc || maxc=$(val="4" min="2" max="16" title='aria2c' gui_slider); [[ -z "$maxc" ]] && maxc=4
	cd "$downdir" && echo "$link" > aria2_filelist && $term aria2c --summary-interval=1800 --file-allocation=none -UMozilla/5.0 -x $maxc -s $maxc -k 10M -i ./aria2_filelist
	;;
	*m3u*)
	text='Specify link or path to m3u/pls file: '; [ -t 0 ] && read -rp "$text" str || str="$(gui_textentry)"; [[ -z "$str" ]] && continue
	[[ -f "$str" ]] && jcp="$(cat "$str")" || jcp="$(curl -m1 -s "$str")"; [[ -z "$jcp" ]] && return 1
	mkdir -p "$downdir/Recordings" && cd "$_" || return 1
	[[ "$str" == *.pls ]] && grep -E '^Title.*=|^File.*=' <<< "$jcp" | tr -d '#)(][}{' | sed -e "/Title.*=/s/\// /g; /Title.*=/s/[0-9]/ /g; s/File.*=//g; s/Title.*=/ out=$(date '+%d-%b-%Y-%H:%M-')/" > aria2_filelist
	[[ "$str" == *.m3u ]] && tac <<< "$jcp" | grep -E '^#EXTINF.*|^http.*' | tr -d '#)(][}{' | sed -e "/EXTINF.*/s/\// /g; /EXTINF.*/s/[0-9]/ /g; s/EXTINF.*/ out=$(date '+%d-%b-%Y-%H:%M-')/" > aria2_filelist
	$term aria2c --summary-interval=3600 -UMozilla/5.0 --auto-file-renaming=false -j 50 -i ./aria2_filelist
find . -maxdepth 1 -type f | while read -r i; do mv "$i" "$i.$(mediainfo -f "$i" | grep -iE 'format|type|extension' | grep -oE 'aac|og[gav]|flac|mp[34]|mpeg|mk[va]' | sed 's/mpeg/mp3/; s/og.*flac/flac/' | head -n1)"; done # get extension using mediainfo
	;;
	*ffmpeg*)
	[ -t 0 ] && continue; mkdir -p "$downdir/Audio_extracted" && cd "$_" || return 1
	link=$(text='Enter link for a single stream, no playlists:' gui_textentry)
	[[ -n "$link" ]] && case $(zenity --list 'Vorbis (specify quality)' 'OPUS (specify bitare)' 'MP3 (specify bitrate)' 'AAC (ffmpeg inbuilt) (specify bitrate)' --column='Sort') in
		Vorbis*)
		ffq=$(val="8" min="0" max="10" text='Specify Vorbis quality, for example 10 is ~500k, 7 is ~224k. It excels at high bitrates:' title='ffmpeg' gui_slider); [[ -z "$ffq" ]] && return 0
		$term ffmpeg -i "$link" -acodec libvorbis -q:a $ffq audio.oga;;
		OPUS*)
		ffb=$(val="96" min="32" max="512" text='OPUS is best at lower bitrates, especially at 64-96k:' title='ffmpeg' gui_slider); [[ -z "$ffb" ]] && return 0
		$term ffmpeg -i "$link" -acodec libopus -b:a "$ffb"k audio.oga;;
		MP3*)
		ffb=$(val="192" min="96" max="320" text='MP3 is a legacy codec, excels at 192k and above:' title='ffmpeg' gui_slider); [[ -z "$ffb" ]] && return 0
		$term ffmpeg -i "$link" -acodec libmp3lame -b:a "$ffb"k audio.mp3;;
		AAC*)
		ffb=$(val="128" min="64" max="320" text='Native ffmpeg AAC encoder used instead of libfdk, you need higher bitrates, above 128k sounds good:' title='ffmpeg' gui_slider); [[ -z "$ffb" ]] && return 0
		$term ffmpeg -i "$link" -acodec aac -b:a "$ffb"k audio.aac
	esac
	;;
	*yt-dlp*)
	if [[ -f "$DIR"/yt-dlp ]]; then ytdlp="$DIR"/yt-dlp; elif [[ -f "$userhome"/bin/yt-dlp ]]; then ytdlp=$userhome/bin/yt-dlp; elif testcmd yt-dlp; then ytdlp=yt-dlp; else return 1; fi
	inputlink; [[ -z "$link" ]] && continue
	cd "$downdir" && $term $ytdlp "$link"
	;;
	*Exit) return 130;;
	*Main*|"") returntomain
esac
done
;;

-span)
checktermgui $1 && return
while menu_OK; do
getsmm() { createarray <<<"$userhome (home directory)
$(drives | xargs -r -P 2 lsblk -n -o MOUNTPOINT | xargs -r -P 2 df -h | awk NR\>1'{print $6" ("$2, $5" used)"}')
 -> Main menu
 -> Exit"; }
title='Disk space analyzer' sel getsmm $szi
case $opt in *Main\ *|"") returntomain;; *Exit) return 130;; *) location=$(cut -d' ' -f1 <<<"$opt") span_mode=1 file_browser; esac
done
;;

-fm)
checktermgui $1 && return
fm_mode=1 file_browser
;;

*)
if runningasroot; then args_rootonly "$@"; else argcontinue=false args_useronly "$@"; ! $argmatched && has_sudoers_attr && sudo "$script_path" "$@"; fi
esac
}

################################################## auxiliary components

# system info part 3

# audio stats
adevs() { [[ -d /proc/asound ]] && find /proc/asound -type f -name hw_params | while read -r s; do [[ "$(cat $s)" != closed ]] && echo $s; done; }; getastats() { astats=$(adevs | awk 'END{if (NR>0) print NR" ALSA active"}' & ! runningasroot && testcmd pw-top && wpctl status 2>$quiet | grep -cE '^ {8}[0-9].* {1,}$' | sed '/^[1-9]$/{s/$/ WP stream/;h}; /^[2-9].*/{s/$/s/;h}; /^0$/{s///;h;q}' & pw-top -bn1 2>$quiet | grep -c '_output' | sed '/^[1-9]$/{s/$/ PW output/;h}; /^[2-9].*/{s/$/s/;h}; /^0$/{s///;h;q}'); }
# gpu stats
gpustats() { cd /sys/class/drm/card0/device 2>$quiet || cd /sys/class/drm/card1/device 2>$quiet || return 1; case $1 in info) gpuinfo=$(lspci -d ::0300 | grep -oiE 'amd.*(polaris|navi)'); return;; show) [[ -f gpu_busy_percent ]] && amdgpuperc=$(cat gpu_busy_percent); igfreq=$(cd ..; cat gt_cur_freq_mhz)
[[ -n $igfreq ]] && echo -ne "[IGPU] freq.: $igfreq MHz, idle total: $(echo "$(( $(cd ..; cat ./power/rc6_residency_ms || echo 1) / 10 / ( $(cut -d. -f1 </proc/uptime) + 10 ) ))%" & runningasroot && sed -z 's/.$//; s/\n/, /g; s/^/, /' /sys/kernel/debug/dri/*/i915_runtime* | head -c45)\n"
[[ -n $amdgpuperc ]] && { cat ./hwmon/*/in0_input ./hwmon/*/power1_average & echo "$(($(cat mem_info_vram_used)/1024/1024))/$(($(cat mem_info_vram_total)/1024/1024))M vram" & runningasroot && grep -o '[0-9]* M.z (SCLK)' </sys/kernel/debug/dri/${PWD//[a-z\/]/}/amdgpu_pm_info || grep -o '[0-9]*M.z \*' pp_dpm_sclk || sed -e '$!d; s/^.*: *//' pp_dpm_sclk; } | awk 'BEGIN{ printf "[AMDGPU] load: '$amdgpuperc'%" }; /M.z/{if ((int($1)) > 50) printf ", freq: "int($1)"MHz"; else printf ", freq: idle"}; /^[0-9]{1,4}$/{if ($1>50) printf ", "$1" mv"; else printf ", v. stdby."}; /vram/{printf ", "$0}; /^[0-9]{6}$/{printf ", pwr. ~"($1 / 1000000)"W"}; END{printf "\n"}'; esac
}
# get formatted sensors info
getsensors() ( sensors -A | sed -e "s/sensor = ...*$//g; s/intrusion.://g; s/ALARM//g; \
s/:  /:/g; s/=  /= /g; s/^fan.*\s0 RPM.*(.*)//g; s/ *(.*)//g; s/^temp.*\sN.A.*//g; s/^temp.*\s127.*//g; \
s/min.*0.00 V, //g; s/.*-[a-z].*-.*/[ Sensor: & ] ↓/g" & gpustats show 2>$quiet )

# check necessary components
getnp() { tc=$( for p in sed awk; do testcmd $p || echo "$p - needed" & done &
testcmd perl || echo 'OPTIONAL: perl - for downloaders, firewall mgr.(hit stats)' &
testcmd iotop || echo 'OPTIONAL - iotop - part of the system monitor commands' &
testcmd busybox && ! testcmd numfmt && ! testcmd gawk && echo 'OPTIONAL: on some busybox distros, coreutils and/or standalone gawk are needed for some functions' &
testcmd nft || echo 'OPTIONAL: nftables - firewall mgr.' &
testcmd hdparm || echo 'OPTIONAL: hdparm - disk status, benchmark and sleep' &
for p in tune2fs sudo lsattr getent zramctl ss column lspci free memtester dmidecode inxi smartctl udisksctl; do testcmd $p || echo "OPTIONAL: $p" & done
); }

# create dynamic menu array from stdin
createarray() { [ "$1" = add ] && mapfile -t menu2 - && menu+=("${menu2[@]}") || mapfile -t menu -; }

# Option selector - selects from array named 'menu' or, for autorefreshing menus, from a function that creates said array specified as the first argument ('menudelay' overrides default autorefreshing rate); takes description text from variables 'title' and 'header' and outputs to variable 'opt'.

sel() {
# determine arguments
if [[ ! "$1" =~ [0-9] ]] && type "$1" >$quiet 2>&1; then func=$1 aref=true w=$2 h=$3; $func; else func=false aref=false w=$1 h=$2; fi; [[ -z "$header" ]] && header=$generictext
if [ -t 0 ]; then
	## terminal mode
##                #first arr elem#                                                              nolinewrap qkclrscr
initselopt=0; [[ ${#menu[0]} -lt 3 ]] && initselopt=1; selopt=$initselopt; $scrollback && clf='\e[?7l\e[1;3J' || clf='\e[?7h\e[J'
	# workaround for searching, determine some vars.
shopt -s lastpipe; ! $autorefresh && [[ -z $force_aref ]] && aref=false; $aref || unset sysinfo; t_text=" $title - $branding "; tstring="[ $user_curr"; [[ -n $SSH_TTY ]] && tstring+="@$(hostname)"; [ "$user_curr" != "$user_tgt" ] && tstring+=" (target: $user_tgt)"; tstring+=" ]"
[[ -n $customtheme ]] && color1=$(cut -d'+' -f1 <<<$customtheme) color2=$(cut -d'+' -f2 <<<$customtheme) || color1='40;140;170' color2='245;220;40'
while menu_OK; do cols=$(getcols) elnr=$(( ${#menu[@]} - $([[ "${#menu[-1]}" -lt 3 ]] && echo 2 || echo 1) )) vpad=
##        home      nocur  bold   rgbtitlebackgr.      padding                 goto->|                      text           goto|<-  text      rgbclr              goto middle                     rgbheaderforegr. nextline+clr text clrfmt clrline->|
echo -e '\e[H'$clf'\e[?25l\e[1m\e[48;2;'$color1'm'"$(printf %"$cols"s)"'\e['$(( $cols - 12 ))'G'"$($aref && date '+%a %R')"'\e[G'$tstring'\e[49m\e['$(( ( $cols - ${#t_text} ) / 2 ))'G'$t_text'\e[38;2;'$color1'm\e[1E\e[2K'$header'\e[0m\e[K' # prepare text feed, output title and header
	# determine scrollback and view boundaries
	if $scrollback && [[ -z "$msrc" ]]; then avail_lines=$(getlines) lines=$(( $avail_lines - $(echo -e "$header\n" | wc -l) )) initelm=$([[ $selopt -ge $(( $lines - 1 )) ]] && echo $(( $selopt - $lines + 2 )) || echo $initselopt) endelm=$(( $initelm + $lines - 2 )); [[ $endelm -gt $elnr ]] && endelm=$elnr; [[ $endelm -gt $avail_lines ]] || [[ $avail_lines -le 50 ]] && vpad="\e[$avail_lines;0H"; [[ $selopt -lt $initelm ]] || [[ $selopt -gt $endelm ]] && selopt=$initelm; else initelm=$initselopt endelm=$elnr; fi
	# output the options
	seq $initelm $endelm | while read -r i; do [[ -z ${menu["$i"]} ]] && continue; [[ ${#menu["$i"]} -lt 3 ]] && menu["$i"]='' && continue; [[ -n "$msrc" ]] && shopt -s nocasematch && [[ "${menu["$i"]}" == *$msrc* ]] && selopt=$i && unset msrc && shopt -u nocasematch && clear
##     #current option#       bold  rgboptbackgr.  rgboptforegr.     clrfmt                           clrline      text   clrline if multiline
[ "$i" = "$selopt" ] && bef='\e[1;38;2;'$color2';48;2;'$color1'm' aft='\e[0m' || unset bef aft; echo -e "\e[2K$bef${menu[$i]//$'\n'/$'\n'$'\e[2K'}$aft"; done || exit 1
##                   home clscr⭳ enabcur                                                                            clscr⭳       bold  rgbfooterbackgr.       padding    goto begin                                          text                              goto near end         text      clrfmt home
resetscr() { echo -e "\e[H\e[J\e[?25h"; }; retsl() { resetscr; opt=${menu[$selopt]} nloops=1 header=; }; echo -e '\e[J'"$vpad"'\e[1m\e[48;2;'$color1'm'"$(printf %"$cols"s)"'\e[0G'"$([[ -n $sysinfo ]] && echo $sysinfo || echo $sysinfo_static)"'\e['$(( $cols - 10 ))'G#'$selopt'/'$elnr'\e[0m\e[H' # function to reset on return, output footer
	# read the input and make the selection
	unset key key1b msrc; if $aref; then read -r -s -n1 -t$menudelay key1b; else read -r -s -n1 key1b; fi
case $key1b in
"") if [[ $? != 0 ]]; then $aref && unset menu && $func && [ "$func" != 'mainmenu' ] && getsysinfo; else opt="${menu["$selopt"]}"; retsl; return 0; fi;; # autorefresh/select (Enter)
[0-9a-zA-Z]) read -r -s -t0; selopt=$initselopt; msrc=$key1b; read -r -s -n1 -t0.5 keys2b && msrc+=$keys2b && read -r -s -n1 -t0.3 keys3b && msrc+=$keys3b;; # quick search
*) [[ $key1b != [[:alnum:]] ]] && read -r -s -n2 key && key3b=$key1b$key &&
case $key3b in
	$'\x1b[A') [ $selopt -ne $initselopt ] && let selopt=selopt-1 && until [ $selopt = $initselopt ] || [[ -n ${menu[$selopt]} ]]; do let selopt=selopt-1; done;; # up
	$'\x1b[B') [ $selopt -ne $elnr ] && let selopt=selopt+1 && until [ $selopt = $elnr ] || [[ -n ${menu[$selopt]} ]]; do let selopt=selopt+1; done;; # down
	$'\x1b[H') selopt=$initselopt;; # top
	$'\x1b[F') selopt=$elnr;; # bottom
	$'\x1b[C') retsl; return 0;; # select (right)
# auxiliary functions (some duplicated for different terminals)
	$'\x1b[D') retsl; unset opt; return 0;; # return (left)
	$'\x1bOP') resetscr; help_text; termslp;; # help text (F1)
	$'\x1bOQ') retsl; unset opt; selmain;; # go to main menu (F2)
	$'\x1bOR') fv=sel; retsl; return 0;; # for file browser (F3)
	$'\x1bOS') fv=ren; retsl; return 0;; # for file browser (F4)
	*) read -r -s -n1 key && key4b=$key3b$key &&
	case $key4b in
		$'\x1b[[A') resetscr; help_text; termslp;; # F1
		$'\x1b[[B') retsl; unset opt; selmain;; # F2
		$'\x1b[[C') fv=sel; retsl; return 0;; # F3
		$'\x1b[[D') fv=ren; retsl; return 0;; # F4
		$'\x1b[[E') resetscr; $func && [[ $func != mainmenu ]] && getsysinfo;; # manual refresh (F5)
		$'\x1b[1~') selopt=$initselopt;; # top
		$'\x1b[4~') selopt=$elnr;; # bottom
		$'\x1b[2~') resetscr; selopt=$initselopt; read -rp 'Enter a search query: ' q; [[ -n $q ]] && msrc=$(cut -c2- <<<$q);; # search (Ins)
		$'\x1b[3~') fv=del; retsl; return 0;; # for file browser (Del)
		$'\x1b[5~') [[ -n ${menu[$(( $selopt - 5 ))]} ]] && let selopt=selopt-5;; # PgUp
		$'\x1b[6~') [[ -n ${menu[$(( $selopt + 5 ))]} ]] && let selopt=selopt+5;; # PgDn
		*) read -r -s -n1 key && key5b=$key4b$key &&
		case $key5b in
			$'\x1b[15~') resetscr; $func && [[ $func != mainmenu ]] && getsysinfo;; # F5
			$'\x1b[17~') fv=pre; retsl; return 0;; # for file browser (F6)
			$'\x1b[18~') resetscr; read -rp "Specify user@host to run script under: " uh; read -rp "Specify keyfile or leave empty for pass: " kf; [[ -n $kf ]] && kf="-i $kf"; ssz=$(du -b $script_path | cut -f1) snm=$(basename $script_path); ssh $kf -nNf -o ControlMaster=yes -o ControlPath=/tmp/$uh $uh; ssh $kf -o ControlPath=/tmp/$uh $uh 'head -c '$ssz' > /tmp/'$snm'' <$script_path; ssh $kf -o ControlPath=/tmp/$uh -tt $uh 'bash /tmp/'$snm'';; # remote conn. (F7)
			*) read -s -t0
		esac; esac; esac; esac; done
	## gui mode, adjust the array and text fields for kdialog/zenity and export the variable
elif testcmd kdialog; then
for i in "${menu[@]}"; do [[ "${#i}" -lt 3 ]] && continue; karr+=( "$i" ); karr+=( "$i" ); karr+=( "off" ); done; opt=$($(runningasroot && echo -n $runfromroot) kdialog --geometry "$w"x"$h"+35+35 --title "$title - $branding" --radiolist "$header" "${karr[@]}" 2>$quiet); unset karr nloops
elif testcmd zenity; then
for i in "${menu[@]}"; do [[ "${#i}" -lt 3 ]] && continue; zarr+=( "$i" ); done; opt=$($(runningasroot && echo -n $runfromroot) zenity --list "${zarr[@]}" --column="Sort" --text="$header" --title="$title - $branding" --width=$w --height=$h 2>$quiet); unset zarr nloops
else exit 1; fi
}

# File browser "API" for terminal, by defaule runs in file chooser mode and outputs to variable 'opt_file'

file_browser() {
choosemode() { [[ -z $fm_mode ]] && [[ -z $span_mode ]]; }; choosemode && if [ ! -t 0 ]; then opt_file=$(gui_filechooser "$@"); return; fi # redirect to the gui chooser if outside a terminal
[[ -z $location ]] && { [ "$PWD" = /tmp ] && location=$userhome || location=$PWD; }; cd $location || return 1; initial_location=$location
while menu_OK; do
fv=; dirsize=$([ "$PWD" = / ] || [ "$PWD" = /home ] && return; [ "$PWD" != "$initial_location" ] || [[ -n $span_mode ]] && du -sb . | cut -f1 | head -n1) title="$(basename "$PWD" | sed 's/^.*$/[&]/g')" header="[HOTKEYS: F3-$(choosemode && echo -n select || echo -n info/edit; testcmd mpv && echo -n '; F6-preview'); F4-rename; Del] $(! choosemode && echo -n "$(df -hT . | awk END'{print "[filesystem type "$2", used "$4" / "$3"]"}') [Current path: $PWD $([[ -n $dirsize ]] && numfmt --to=iec --suffix=B $dirsize)]"; [[ -n $text ]] && echo -e "\n	[ $text ]" && separator /2)"
createarray <<<"
 -> Up
 -> Refresh
$(choosemode && echo -e ' -> Input full path manually\n -> Save new file\n -> Open in GUI' || echo -n ' -> Find large files')
$(separator; if [[ -n $span_mode ]]; then if [[ -n $findlarge ]]; then find . -maxdepth 3 -size +30M -print0; else find . -mindepth 1 -maxdepth 1 -size +0 -print0; fi 2>$quiet | xargs -r -0 -P 2 du -sb | while read -r f; do awk -v CONVFMT='%.2f' -v dsz="$dirsize" -v fsz="$(numfmt --to=iec --suffix=B "$f" 2>$quiet)" '{print "["sprintf($1 * 100 / dsz)" %]",fsz}' <<<"$f" & done | sort -rhk2
else find . -mindepth 1 -maxdepth 1 -type d -print0 | while read -r -d $'\0' i; do echo "[ dir ] $i"; done && find . -mindepth 1 -maxdepth 1 -not -type d -print0 | while read -r -d $'\0' i; do if [[ -L "$i" ]]; then echo "[ link ] $i"; else echo "[ $(du -h "$i" | cut -f1 | head -n1) ] $i"; fi & done | sort; fi; separator /2)
 -> Back to home"
sel $szi
fsl="$(cut -d' ' -f4- <<<"$opt")"; unset findlarge
case $fv in
sel) if choosemode; then opt_file=$(realpath $fsl) && break; else info=$(file $fsl); ! grep -q ' text' <<<$info && echo $info && termslp || nano $fsl; fi;;
pre) testcmd mpv && mpv --vo=tct "$fsl";;
ren) read -rp "Enter new name for file $(basename "$fsl"): " fren; [[ ${#fren} -ge 2 ]] && mv $fsl "$(dirname "$fsl")/$fren";;
del) read -rp "Do you want to delete $(basename "$fsl")? [y/N] " fdel; if menu_OK && [[ -n $fdel ]] && [[ $fdel =~ y|Y ]]; then rm -rvf "$fsl" || (! runningasroot && $sub rm -rvf "$fsl"); sleep .5; fi;;
*) case $opt in
	' -> Up') [[ $PWD = '/' ]] && break; cd ..; [[ -n $span_mode ]] && [ "$PWD" = "$( dirname "$initial_location")" ] && break;;
	*\>\ Find\ large\ files) findlarge=1;;
	*\>\ Input\ *manually) read -rp 'Input location: ' opt_file; break;;
	*\>\ Save\ new\ *here) read -rp 'Enter name for new file: ' tfile; opt_file="$PWD"/"$tfile"; break;;
	*\>\ Open\ in\ GUI) opt_file=$(gui_filechooser "$@"); break;;
	*\>\ Refresh) continue;;
	' -> Back to home'|"") break;;
	*) if [[ -f "$fsl" ]] && choosemode; then opt_file=$(realpath $fsl) && break; elif [[ -d $fsl ]]; then location=$PWD; cd "$fsl" || continue; else continue; fi
esac; esac; done
}

# extra gui components, not needed for terminal mode

gui_filechooser() {
# usage: choose or save files or directories, outputs result to stdout
if [ "$1" = '--dir' ]; then
if testcmd kdialog; then
$(runningasroot && echo -n $runfromroot) kdialog --getexistingdirectory $userhome --title="$text"
else $(runningasroot && echo -n $runfromroot) zenity --file-selection --filename="$userhome/." --title="$text" --save --directory; fi
else
if testcmd kdialog; then
if [ "$1" = '--save' ]; then $(runningasroot && echo -n $runfromroot) kdialog --getsavefilename "$userhome" --title="$text"; else $(runningasroot && echo -n $runfromroot) kdialog --getopenfilename $userhome --title="$text"; fi
else $(runningasroot && echo -n $runfromroot) zenity --file-selection --filename="$userhome/." --title="$text" $1; fi
fi 2>$quiet; }

gui_textentry() {
# usage: outputs to stdout
if testcmd kdialog; then
$(runningasroot && echo -n $runfromroot) kdialog --title="$title" --textinputbox "$text"
else $(runningasroot && echo -n $runfromroot) zenity --entry --text="$text" --title="$title"; fi 2>$quiet; }

gui_slider() {
# usage: outputs to stdout
if testcmd kdialog; then
$(runningasroot && echo -n $runfromroot) kdialog --title="$title" --slider "$text" ${min:?} ${max:?} ${val:?}
else $(runningasroot && echo -n $runfromroot) zenity --scale --value=$val --min-value=$min --max-value=$max --text="$text" --title="$title"; fi; }

gui_warn() {
# usage: returns exit code 0/1
if testcmd kdialog; then
$(runningasroot && echo -n $runfromroot) kdialog --title="$title" --warningcontinuecancel "$text"
else $(runningasroot && echo -n $runfromroot) zenity --title="$title" --warning --text="$text"; fi; }

gui_question() {
# usage: returns exit code 0/1
if testcmd kdialog; then
$(runningasroot && echo -n $runfromroot) kdialog --title="$title" --yesno "$text"
else $(runningasroot && echo -n $runfromroot) zenity --title="$title" --question --text="$text"; fi; }

gui_textbox() {
# usage: opens from stdin
w=$(cut -d' ' -f1 <<<$1) h=$(cut -d' ' -f2 <<<$1)
if testcmd kdialog; then
$(runningasroot && echo -n $runfromroot) kdialog --title="$title" --geometry "$w"x"$h"+35+35 --textbox -
else $(runningasroot && echo -n $runfromroot) zenity --title="$title" --text-info --width=$w --height=$h; fi; return 0; }

# print out help and debug text

help_text() {
echo "
Console navigation and hotkeys:
* Up/down arrow: go up/down;
* Right arrow or Enter: select an option;
* Left arrow: cancel;
* Home/End: go to the top/bottom;
* PgUp/PgDn: skip 5 rows;
* Ins: search;
* F2: go to main menu from any page;
* F5: refresh;
* F7: Run the script on a remote machine over ssh (it will be placed in it's /tmp).
* (F3,F4,F6: used in the file manager)


List of arguments:

Universal:
* [No arg]: launch main menu GUI or the option selectors if inside a terminal;
* -audiomgr: list specs and open alsamixer to specified audio device, list and control volume of pipewire streams;
* -arc: Archiving and backup tools [ optional vars. for automation: opt_file='/full/path' and opt_dir='/full/path' ];
* -dltools: Various downloaders based on wget/aria2/curl/httrack/etc;
* -fm: File manager;
* -iomons: Sensors and I/O monitors;
* -pkgutils: Package utilities for Pacman and embedded system mgrs. apk/opkg;
* -prefs: Settings menu and management of script's integration in the system;
* -span: Disk space analyzer.

Non root:
* -customcommands: Run quick commands from parsed .desktop entries, your command history or 'custom_commands' file;
* -wine (gui only): Wine custom version and prefix manager + dxvk installer;
* -disptools: various display mgmt. tools: for X11 and Wayland.

Root:
* -addrule/-rmrule/-testrule: Manage the sudoers rule for this script;
* -diags: System diagnostics and info;
* -fancontrol: Universal utility that auto detects running fans;
* -fw: nftables and xtables firewall manager, with ip blocking filter [ runs automatically with second argument 'ipfilter'; custom_ips file next to script adds ip's, any uncompressed format ] and standalone wireguard connection tools [ connects automatically if a .conf file is specified in the wg_file variable at runtime ];
* -kern: Kernel tools (modules, parameters, grub config/installation, dkms refresh and info);
* -maint: System maintenance menu with cleanup, defrag and fstrim commands;
* -mnt: Universal mounting utility that auto detects mountable drives [ auto mounts with second argument 'automount', extra mount options in third argument ];
* -hwp: Set cpu, gpu frequencies and governor/profile, io schedulers and overview;
* -srv: Service manager for OpenRC, Dinit, Runit, s6-init;
* -swp: Swap manager [second argument 'zram' creates a mixed zram volume totaling 1/4 of ram];
* -usb: USB device manager.


Additional parameters:
*run with 'nouser=1' to avoid determining paths or display values (for usage in boot scripts);
*run with 'source=1' for sourcing main functions of the script;
*run with 'user_tgt=...' to set the target user at runtime.

"
[ -t 0 ] && read -rsn1 -p 'Press any key for [debug info]...' 2>&1 || echo -e '[Debug info]\n'
unset sysinfo var; typeset | grep -E "$(grep -oE '\$([a-z]{2,}_[a-z]{1,}|[a-z]{2,})|=\$[a-z]{2,}' "$script_path" | sed -e 's/^.*$/^&=/g; s/\$//g' | tr '\n' '|' | sed 's/.$//')" | sort -t'=' -k2n
}

################################################## run the script

[[ -n $source ]] && exit; if [[ -n $1 ]]; then [[ -z $nouser ]] && getnp; args_all "$@"; else selmain; fi 2> >( writelog )
