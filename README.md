![Test gif of V14](https://gitlab.com/Hitman_47/utilities.sh/-/raw/main/screenshots/V14.gif)


### *System utilities for your favourite shell!*


Universal shell script with various utilities meant to simplify a lot of common system administration jobs or just the day to day work around an embedded distribution - and even a full featured distro where some tools are less of a mess -, with easy to understand menus onto a custom menu system, written almost entirely by myself. It tries to require as few dependencies as possible and the features are reasonably portable and customizable, generally non invasive to the system with well defined exceptions.
##### Features: Custom menu system, elegant terminal menus, Zenity/Qarma/KDialog GUI support, SSH deploy.


It comes in two versions, one is posix-friendly bash, and the other ZSH, on which some native features are used for improved performance.


This script can be launched as a normal user or as root, inside of a terminal or outside of one. Running it with no arguments launches the option selectors and/or the GUI. If a root option is selected, the script will launch another instance of itself as root to take the argument, which allows easy creation of a sudoers rule. It also does vice-versa when needed, e.g. for GUI components in a root option, or for running the script as a normal user. More in depth documentation coming soon!


- Terminal emulators supported: alacritty, kgx, kitty, konsole, lxterminal, qterminal, terminator, tilix, xfce4-terminal
- Password prompt programs supported: pkexec, lxqt-sudo


##### *List of arguments + feature map*


Universal:
* [No arg]: launch main menu GUI or the option selectors if inside a terminal;
* -audiomgr: list specs and open alsamixer to specified audio device, list and control volume of pipewire streams;
```
Select an option for volume control:
ALSA devices active:
<...>
Pipewire streams active:
 -> Change main rate to 44.1KHz
 -> Change main rate to 48KHz
 -> Change main rate to 96KHz
<...>
```
* -arc: Archiving and backup tools [ optional vars. for automation: opt_file='/full/path' and opt_dir='/full/path' ];
```
Supported formats:
For packing/adding to existing archive: .7z, .zip
For packing only(backups): .tar.bz2 .tar.gz .tar.xz [Uses 7zip binary as compressor] [Tar exclusion lists supported]
For unpacking: Anything suppored by the 7zip and tar binaries
 -> Pick an existing archive file
 -> Create new archive (full - multi source)
 -> Create new archive (quick - single source)
 -> (quick) Home directory backup for user <...>
 ```
* -dltools: Various downloaders based on wget/aria2/curl/httrack/etc;
```
<location/stats>
 -> [httrack] Download/mirror websites
 -> [wget] Download files listed on a website
 -> [aria2] Download a file with multiple connctions
 -> [aria2] Rip online stream(s) from m3u/pls playlist
 -> [yt-dlp] Download a video or playlist
 ```
* -fm: File manager;
```
<hotkeys/mode/location/fs type/free space>
<...>
```
* -iomons: Sensors and I/O monitors;
```
 <amdgpu/i915 load/stats>
 <I/O Summary>
 <Top processes by CPU/RAM>
 <I/O per process>
```
* -pkgutils: Package utilities for Pacman and embedded system mgrs. apk/opkg;
```
<stats>
<...>
 -> Search/display info - Installed packages
 -> Search/display info - Available packages
 # -> Install package(s)
 # -> Remove package(s)
 # -> Update packages
 # -> Force update packages, allowing downgrades
 # -> Audit file changes
 # -> Clean caches
 <...>
```
* -prefs: Settings menu and management of script's integration in the system;
```
    Terminal emulator of choice
 <...>
    Show optional packages: No
    Console - autorefresh: Yes
    Console - scrollback (disabling also turns off quick screen refresh, use in case of issues): Yes
    Use GUI mode where possible: Yes
    -> Edit RGB for custom theme [current: default]
 <...>
 -> Show help and debug info
```
* -span: Disk space analyzer.


Non root:
* -customcommands: Run quick commands from parsed .desktop entries, your command history or 'custom_commands' file;
```
 -> [ Run directory: /tmp; select to change ]
 <...>
 [ Preset - mozilla profiles found ]
 <...>
 [Launchers in your homedir]
 <...>
 [Parsed from .desktop entries]
 <...>

 # Menu format:

 # plaintext for raw commands, or:
 # !titles
 # @options
 # %case command or equivalent, to run based on selected text ($opt); parsed for all options

```
* -wine (gui only): Wine custom version and prefix manager + dxvk installer;
```
 -> Mode: Run .exe (default) *
 -> Mode: Run winecfg
 -> Mode: Run control panel
 -> Mode: Install dxvk in a prefix
 <...>
```
* -disptools: various display mgmt. tools: for X11 and Wayland.

Root:
* -addrule/-rmrule/-testrule: Manage the sudoers rule for this script;
* -diags: System diagnostics and info;
* -fancontrol: Universal utility that auto detects running fans;
* -fw: nftables and xtables firewall manager, with ip blocking filter [ runs automatically with second argument 'ipfilter'; custom_ips file next to script adds ip's, any uncompressed format ] and standalone wireguard connection tools [ connects automatically if a .conf file is specified in the wg_file variable at runtime ];
* -kern: Kernel tools (modules, parameters, grub/refind mgmt., dkms refresh and info);
* -maint: System maintenance menu with cleanup, defrag and fstrim commands;
* -mnt: Universal mounting utility that auto detects mountable drives [ auto mounts with second argument 'automount', extra mount options in third argument ];
* -hwp: Set cpu, gpu frequencies and governor/profile, io schedulers and overview;
* -srv: Service manager for OpenRC, Dinit (flagship support for user services with Turnstile), Runit, s6-init;
* -swp: Swap manager [second argument 'zram' creates a mixed zram volume totaling 1/4 of ram];
* -usb: USB device manager.


Console navigation and hotkeys:
* Up/down arrow: go up/down;
* Right arrow or Enter: select an option;
* Left arrow: cancel;
* Home/End: go to the top/bottom;
* PgUp/PgDn: skip 5 rows;
* Ins: search;
* F2: go to main menu from any page;
* F5: refresh;
* F7: Run the script on a remote machine over ssh (it will be placed in it's /tmp).
* (F3,F4,F6: used in the file manager)


Additional parameters:
* run with 'nouser=1' to avoid determining paths or display values (for usage in boot scripts);
* run with 'source=1' for sourcing main functions of the script;
* run with 'user_tgt=...' to set the target user at runtime.
